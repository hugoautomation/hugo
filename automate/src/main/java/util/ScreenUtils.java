package util;

import com.google.common.collect.ImmutableList;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AuthenticatesByFinger;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import pages.HomePage;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Pause;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static driver.DriverManager.DRIVER;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public interface ScreenUtils {
	public static final long WAIT = 60;

	static void scrollDownTimes(int times) throws InterruptedException {
		TimeUnit.SECONDS.sleep(1);
		for (int i = 0; i < times; i++) {
			scrollDown(500);
		}
		TimeUnit.SECONDS.sleep(1);
	}

	static void scrollDown(int pixelsToScroll) {
		Point targetPoint = new Point(getRightCenterScreenBoundary(), getYAxisScreenCenter());
		TouchAction<? extends WebElement> action = new TouchAction<>(DRIVER.getDriver());
		action.press(point(targetPoint)).waitAction(new WaitOptions().withDuration(ofMillis(1300L)))
				.moveTo(point(getRightCenterScreenBoundary(), getYAxisScreenCenter() - pixelsToScroll)).release()
				.perform();
	}

	static WebElement findItemWithScrollingUsingBy(WebElement element, int interactions) {
		
		for (int i = 0; i < interactions; i++) {
			if (!element.isDisplayed()) {
				scrollDown(600);
			} else {
				return element; // you can add .click() here instead of returning the element
			}
		}
		Assert.fail("Element not found");
		return null;
	}
	
	
	static void findItemUsingBy(WebElement element1, WebElement element2,int interactions) {
		for (int i = 0; i < interactions; i++) {
			if (!element2.isDisplayed()) {
				swipeElementAndroid(element1, "LEFT");
			}
				if (!element2.isDisplayed()) {
				scrollDown(500);
				
				}		
		
		}
	}
	
	static WebElement findItemUsingBy2(WebElement element1, WebElement element2,int interactions) {
		for (int i = 0; i < interactions; i++) {
			if (!element2.isDisplayed()) {
				swipeElementAndroid(element1, "LEFT");
			}
			else
			{
				return element2;
			}
		}
		return null;
	}
	
	
	static WebElement findItemOnList(WebElement element,int interactions) {
		for (int i = 0; i < interactions; i++) {
				if (!element.isDisplayed()) {
				scrollDown(500);
				}
				else
				{
					return element;
				}
		}
		return null;
		
	}

	static void clickOnPoint(int x, int y) {
		Point targetPoint = new Point(x, y);
		TouchAction<? extends WebElement> action = new TouchAction<>(DRIVER.getDriver());
		action.tap(point(targetPoint)).release().perform();
	}

	static void recoverFocus() {
		Point targetPoint = new Point(getScreenWidth() / 2, getScreenHeight() - 80);
		TouchAction<? extends WebElement> action = new TouchAction<>(DRIVER.getDriver());
		action.tap(point(targetPoint)).perform();
	}

	static int getYAxisScreenCenter() {
		return getScreenHeight() / 2;
	}

	static int getXAxisScreenCenter() {
		return getScreenWidth() / 2;
	}

	static int getRightCenterScreenBoundary() {
		return getScreenWidth() - 1;
	}

	static int getScreenHeight() {
		return DRIVER.getDriver().manage().window().getSize().getHeight();
	}

	static int getScreenWidth() {
		return DRIVER.getDriver().manage().window().getSize().getWidth();
	}

	static int getScrollStepToTop() {
		return (int) (getYAxisScreenCenter() / 0.8);
	}

	static boolean isColorBlackSet(WebElement element) throws IOException {
		File scrFile = ((TakesScreenshot) DRIVER.getDriver()).getScreenshotAs(OutputType.FILE);
		BufferedImage image = ImageIO.read(scrFile);

		boolean ans = false;

		// Getting pixel color by position x and y
		int clr = image.getRGB(element.getRect().x, element.getRect().y);
//        int a = (clr>>24)&0xff;
		int r = (clr >> 16) & 0xff;
		int g = (clr >> 8) & 0xff;
		int b = clr & 0xff;
		System.out.println("R is " + r);
		System.out.println("G is " + g);
		System.out.println("B is " + b);

		if (r == 0 && g == 0 && b == 0) {
			ans = true;
		}

		return ans;
	}
		
	@SuppressWarnings("rawtypes")
	static void swipeElementAndroid(WebElement el, String dir) {
		
	    System.out.println("swipeElementAndroid(): dir: '" + dir + "'"); // always log your actions
	    final int ANIMATION_TIME = 200;
	    final int PRESS_TIME = 200;
	    int edgeBorder;
	    PointOption pointOptionStart, pointOptionEnd;

	    Rectangle rect = el.getRect();

	    edgeBorder = 0;

	    switch (dir) {
	        case "DOWN": // from up to down
	            pointOptionStart = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + edgeBorder);
	            pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + rect.height - edgeBorder);
	            break;
	        case "UP": // from down to up
	            pointOptionStart = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + rect.height - edgeBorder);
	            pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + edgeBorder);
	            break;
	        case "LEFT": // from right to left
	            pointOptionStart = PointOption.point(rect.x + rect.width - edgeBorder,
	                    rect.y + rect.height / 2);
	            pointOptionEnd = PointOption.point(rect.x + edgeBorder,
	                    rect.y + rect.height / 2);
	            break;
	        case "RIGHT": // from left to right
	            pointOptionStart = PointOption.point(rect.x + edgeBorder,
	                    rect.y + rect.height / 2);
	            pointOptionEnd = PointOption.point(rect.x + rect.width - edgeBorder,
	                    rect.y + rect.height / 2);
	            break;
	        default:
	            throw new IllegalArgumentException("swipeElementAndroid(): dir: '" + dir + "' NOT supported");
	    }

	    // execute swipe using TouchAction
	    try {
	        new TouchAction(DRIVER.getDriver())
	                .press(pointOptionStart)
	                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
	                .moveTo(pointOptionEnd)
	                .release().perform();
	        
	       
	    } catch (Exception e) {
	        System.err.println("swipeElementAndroid(): TouchAction FAILED\n" + e.getMessage());
	        return;
	    }
	    try {
	        Thread.sleep(ANIMATION_TIME);
	    } catch (InterruptedException e) {
	        // ignore
	    }
	}
	
@SuppressWarnings("rawtypes")
static void swipeElementByPixels(WebElement el, String dir) {
		
	    System.out.println("swipeElementAndroid(): dir: '" + dir + "'"); // always log your actions
	    final int ANIMATION_TIME = 200;
	    final int PRESS_TIME = 300;
	    int edgeBorder;
		PointOption pointOptionStart, pointOptionEnd;

	    Rectangle rect = el.getRect();

	    edgeBorder = 0;

	    switch (dir) {
	        case "DOWN": // from up to down
	            pointOptionStart = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + edgeBorder);
	            pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + rect.height - edgeBorder);
	            break;
	        case "UP": // from down to up
	            pointOptionStart = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + rect.height - edgeBorder);
	            pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
	                    rect.y + edgeBorder);
	            break;
	        case "LEFT": // from right to left
	            pointOptionStart = PointOption.point((rect.x+100) + rect.width - edgeBorder,
	                    rect.y + rect.height / 2);
	            pointOptionEnd = PointOption.point(rect.x + edgeBorder,
	                    rect.y + rect.height / 2);
	            break;
	        case "RIGHT": // from left to right
	        	
	            pointOptionStart = PointOption.point((rect.x-100)  + edgeBorder,
	                    rect.y + rect.height / 2);
	            pointOptionEnd = PointOption.point(rect.x + rect.width - edgeBorder,
	                    rect.y + rect.height / 2);
	            System.out.println("pointOptionStart: " + pointOptionStart);
	        	System.out.println("pointOptionEnd: " + pointOptionEnd );
	            break;
	        default:
	            throw new IllegalArgumentException("swipeElementAndroid(): dir: '" + dir + "' NOT supported");
	    }

	    // execute swipe using TouchAction
	    try {
	        new TouchAction(DRIVER.getDriver())
	                .press(pointOptionStart)
	                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
	                .moveTo(pointOptionEnd)
	                .release().perform();
	        
	       
	    } catch (Exception e) {
	        System.err.println("swipeElementAndroid(): TouchAction FAILED\n" + e.getMessage());
	        return;
	    }
	    try {
	        Thread.sleep(ANIMATION_TIME);
	    } catch (InterruptedException e) {
	        // ignore
	    }
	}
	
	
//Swipe Screen
	static void swipe(Duration duration) throws InterruptedException {
		boolean isAndroid = DRIVER.getDriver() instanceof AndroidDriver<?>;
		TimeUnit.SECONDS.sleep(2);

		PointerInput input = new PointerInput(PointerInput.Kind.TOUCH, "finger1");
		Sequence swipe = new Sequence(input, 0);
		swipe.addAction(input.createPointerMove(Duration.ZERO, PointerInput.Origin.viewport(), getScreenWidth() / 2,
				((getScreenHeight() / 12) * 11)));
		swipe.addAction(input.createPointerDown(PointerInput.MouseButton.LEFT.asArg()));
		if (isAndroid) {
			duration = duration.dividedBy(3);
		} else {
			swipe.addAction(new Pause(input, duration));
			duration = Duration.ZERO;
		}
		swipe.addAction(input.createPointerMove(duration, PointerInput.Origin.viewport(), getScreenWidth() / 2,
				((getScreenHeight() / 12) * 3)));
		swipe.addAction(input.createPointerUp(PointerInput.MouseButton.LEFT.asArg()));
		DRIVER.getDriver().perform(ImmutableList.of(swipe));

		TimeUnit.SECONDS.sleep(2);
	}

	@SuppressWarnings("rawtypes")
	static void backAndroidButton() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		((AndroidDriver) DRIVER.getDriver()).pressKey(new KeyEvent(AndroidKey.BACK));
	}

	static void waitForVisibility(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(DRIVER.getDriver(), ScreenUtils.WAIT);
		wait.until(ExpectedConditions.visibilityOf(webElement));

	}
	static void waitForVisibility2(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(DRIVER.getDriver(), 5);
		wait.until(ExpectedConditions.visibilityOf(webElement));

	}
	
	static void waitForInvisibility(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(DRIVER.getDriver(), ScreenUtils.WAIT);
		wait.until(ExpectedConditions.invisibilityOf(webElement));

	}
	
	static void Setlocation() {
		DRIVER.getDriver().setLocation(new Location(13.683254, -89.274384, 932));
	}
	

	@SuppressWarnings("rawtypes")
	public static String readOTP() throws InterruptedException {
		((AndroidDriver) DRIVER.getDriver()).openNotifications();
		// waitForVisibility(DRIVER.getDriver().findElementById("android:id/message_text"));
		Thread.sleep(35000);
		AndroidElement smstxt = (AndroidElement) DRIVER.getDriver().findElementById("android:id/message_text");
		String Code = smstxt.getText().replaceAll("\\D+", "");
		((PressesKey) DRIVER.getDriver()).pressKey(new KeyEvent().withKey(AndroidKey.BACK));
		return Code;

	}

	@SuppressWarnings("deprecation")
	static void ClickKeyboard(String Code) {
		DRIVER.getDriver().getKeyboard().sendKeys(Code);

	}
	
	static void LongPressClick(WebElement webElement) throws InterruptedException {
	
		new Actions(DRIVER.getDriver()).clickAndHold(webElement).perform();

	}
	


	static void SingleTap(WebElement webElement) throws InterruptedException {
		new Actions(DRIVER.getDriver()).doubleClick(webElement).perform();
}
	static void getXandYLocation(WebElement webElement) throws InterruptedException {
		MobileElement element = (MobileElement) DRIVER.getDriver().findElementById("com.hugoapp.client.dev:id/nameCountry");
		Point location = element.getLocation();
		System.out.println(location);
}
	
	
	static void SearchService() {
		for (int i = 0; i <= 4; i++) {
			switch (i) {
			case 1:
				try {
					ScreenUtils.findItemUsingBy(HomePage.recycleView1(), HomePage.specificServiceBtn(), 1);
				} catch (Exception e) {
				}
				break;
			case 2:
				try {
					ScreenUtils.findItemUsingBy(HomePage.recycleView2(), HomePage.specificServiceBtn(), 1);
				} catch (Exception e) {
				}
				break;
			case 3:
				try {
					ScreenUtils.findItemUsingBy(HomePage.recycleView3(), HomePage.specificServiceBtn(), 1);
				} catch (Exception e) {
				}
				break;
			case 4:
				try {
					ScreenUtils.findItemUsingBy(HomePage.recycleView4(), HomePage.specificServiceBtn(), 1);
				} catch (Exception e) {
				}
				break;

			}
		}
	}
	
	static void FingerPrint() {
		((AuthenticatesByFinger) DRIVER.getDriver()).fingerPrint(1);
	}
	
	 public static void copyFile(String from, String to) {
	        copyFile(from, to, Boolean.TRUE);
	    }
	 
	 public static void copyFile(String from, String to, Boolean overwrite) {

	        try {
	            File fromFile = new File(from);
	            File toFile = new File(to);

	            if (!fromFile.exists()) {
	                throw new IOException("File not found: " + from);
	            }
	            if (!fromFile.isFile()) {
	                throw new IOException("Can't copy directories: " + from);
	            }
	            if (!fromFile.canRead()) {
	                throw new IOException("Can't read file: " + from);
	            }

	            if (toFile.isDirectory()) {
	                toFile = new File(toFile, fromFile.getName());
	                
	            }

	            if (toFile.exists() && !overwrite) {
	                throw new IOException("File already exists.");
	            } else {
	                String parent = toFile.getParent();
	                if (parent == null) {
	                    parent = System.getProperty("user.dir");
	                }
	                File dir = new File(parent);
	                if (!dir.exists()) {
	                    throw new IOException("Destination directory does not exist: " + parent);
	                }
	                if (dir.isFile()) {
	                    throw new IOException("Destination is not a valid directory: " + parent);
	                }
	                if (!dir.canWrite()) {
	                    throw new IOException("Can't write on destination: " + parent);
	                }
	            }

	            FileInputStream fis = null;
	            FileOutputStream fos = null;
	            try {

	                fis = new FileInputStream(fromFile);
	                fos = new FileOutputStream(toFile);
	                byte[] buffer = new byte[4096];
	                int bytesRead;

	                while ((bytesRead = fis.read(buffer)) != -1) {
	                    fos.write(buffer, 0, bytesRead);
	                }

	            } finally {
	                if (from != null) {
	                    try {
	                        fis.close();
	                    } catch (IOException e) {
	                      System.out.println(e);
	                    }
	                }
	                if (to != null) {
	                    try {
	                        fos.close();
	                    } catch (IOException e) {
	                        System.out.println(e);
	                    }
	                }
	            }

	        } catch (Exception e) {
	            System.out.println("Problems when copying file.");
	        }
	    }
	 
	 //Agregando cambios
		
	}
	
	

