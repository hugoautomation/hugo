package configuration;

public interface ProjectProperties
{
    String USER_DIR = System.getProperty("user.dir");
    String BUILD_ENVIRONMENT = System.getProperty("build_env", Environment.TEST.env);
    String DEVICE_PLATFORM = System.getProperty("platform", Platform.ANDROID.device);
    String DEVICE_PLATFORM2 = System.getProperty("platform", Platform.IOS.device);
    String DEVICE_PLATFORM3 = System.getProperty("platform", Platform.BROWSERSTACK.device);
    String HUB = System.getProperty("hub", Hub.LOCAL.runType);
}
