
package configuration;

public enum Platform
{
    IOS("ios"),
    ANDROID("android"),
	BROWSERSTACK("browserstack"),
	WEB("web");
    public final String device;

    Platform(String platformType) {
        this.device = platformType;
    }
}
