package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.codeborne.selenide.Condition;


public class SignUp {

	public static String Language = System.getProperty("language");
	public static WebElement SelectLanguage;
	
	 public static WebElement SignUpScreen()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/imgSlide")).shouldBe(Condition.visible);
	    }
	 public static WebElement SkipScreen()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnSkip"));
	    }
	 public static WebElement OnboardingSlide()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/masterwelcome"));
	    }
	 public static WebElement StartOnHugo()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnDoneOnb"));
	    }
	 public static WebElement BackSignUp()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnBackHeaader"));
	    }
	 public static WebElement AreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtCodeArea"));
	    }
	 public static WebElement SearchAreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/editSearchCode"));
	    }
	 public static WebElement ResultAreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/nameCountry"));
	    }
	 public static WebElement TxtPhoneNumber()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtNumberPhone"));
	    }
	 
	 public static WebElement receiveSMSBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonSendMsg"));
	    }
	 public static WebElement receiveWhatsappBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonSendWhatsapp"));
	    }
	 public static WebElement ChkTermBox()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/chkTerm"));
	    }
	 public static WebElement ViewTerms()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/textViewTerms"));
	    }
	 public static WebElement BackTerms()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/back_btn"));
	    }
	 public static WebElement BtnNextStep1()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnDoneStep1"));
	    }
	 
	 public static WebElement btnSendCodeSMS()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonSendMsg"));
	    }
	 public static WebElement btnSendCodeWhatsapp()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonSendWhatsapp"));
	    }
	 
	 public static WebElement BtnLogIn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnDoneStep1"));
	    }
	 public static WebElement BtnHelp()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtHelp"));
	    }
	 public static WebElement BtnContactAnAgent()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnOneSheet"));
	    }
	 public static WebElement BtnBackHugoChat()
	    {
		 	switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]"));
		 		break;
		 	}
		 	return SelectLanguage;
		 	
	    }
	 public static WebElement BtnChatOptions()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.ImageView[@content-desc=\"Mas Opciones\"]"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.ImageView[@content-desc=\"More options\"]"));
		 		break;
		 	}
		 	return SelectLanguage;
	    }
	 public static WebElement BtnEndChat()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/content"));
	    }
	 public static WebElement TxtMessageChat()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/input_box_input_text"));
	    }
	 public static WebElement BtnSendMessageChat()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/input_box_send_btn"));
	    }
	 public static WebElement TxtPinMessage()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/lytCode"));
	    }
	 public static WebElement BtnGetNewCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtGetNewCode"));
	    }
	 //Step 2 of SignUp
	 public static WebElement BtnRequestHelp()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtNeedHelp"));
	    }
	 public static WebElement BtnRequestCodeByCall()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnOneSheet"));
	    }
	 public static WebElement BtnContactAnAgent2()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnTwoSheet"));
	    }
	 public static WebElement BtnThanksHugo()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/button_dialog_box"));
	    }
	 //Error modal when u get code by call
	 public static WebElement BtnErrorAcept()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnAcept"));
	    }
	 
	 public static WebElement txtEdtName()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtName"));
	    }
	 public static WebElement txtLastName()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtLastName"));
	    }
	 public static WebElement txtEmail()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtMail"));
	    }
	 public static WebElement btnDoneStep3()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnDoneStep3"));
	    }
	 public static WebElement btnSuccesAcount()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnSuccessAcount"));
	    }
	 public static WebElement lblErrorAccountExist()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='El n�mero ingresado ya tiene una cuenta activa']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='The number entered already has an active account']"));
		 		break;
		 	}
		 	return SelectLanguage;

	    }
	 public static WebElement btnLogIn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.Button[@text='INICIAR SESI�N']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.Button[@text='LOG IN']"));
		 		break;
		 	}

		 	return SelectLanguage;
		
	    }
	 

	 
	//Agregando cambios
}
