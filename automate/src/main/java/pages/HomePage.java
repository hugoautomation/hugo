package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class HomePage {
	

	public static String Language = System.getProperty("language");
	public static WebElement SelectLanguage;
	public static String Service = System.getProperty("Service");
	public static String Address = System.getProperty("Address");
	public static String SearchResult = System.getProperty("SearchResult");
	public static String Product = System.getProperty("Product");
	public static String CategoryHugoMarket = System.getProperty("CategoryHugoMarket");
	public static String PhoneOperator = System.getProperty("PhoneOperator");
	public static String ServicePagoAServicio = System.getProperty("ServicePagoAServicio");
	public static String Provider = System.getProperty("Provider");
	public static String TypeOfContract = System.getProperty("TypeOfContract");
	public static String ServiceToSwipe1 = System.getProperty("ServiceToSwipe1");
	public static String ServiceToSwipe2 = System.getProperty("ServiceToSwipe2");
	public static String ServiceToSwipe3 = System.getProperty("ServiceToSwipe3");
	public static String ServiceToSwipe4 = System.getProperty("ServiceToSwipe4");
	public static String Name = System.getProperty("Name");
	public static String LastName = System.getProperty("LastName");
	public static String Category = System.getProperty("Category");
	public static String Event = System.getProperty("Event");
	public static String Last4NumberCard = System.getProperty("Last4NumberCard");
	public static String ServiceOfMastersearch = System.getProperty("ServiceOfMastersearch");

	public static WebElement profileBtnHome() {
		//return $(By.id("com.hugoapp.client.dev:id/circleImageViewUserPhoto"));
		return $(By.id("com.hugoapp.client.dev:id/profileInformationFragment"));
	}

	public static WebElement hugoDeliveryText() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='hugoDelivery']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='hugoDelivery']"));
	 		break;
	 	}
	 	return SelectLanguage;
	}

	//------------------------Clevertap--------------------------------------------//	
	public static WebElement clevertapDialog() {
		return $(By.id("com.hugoapp.client.dev:id/half_interstitial_relative_layout"));
	}
	public static WebElement clevertapDialog2() {
		return $(By.id("com.hugoapp.client.dev:id/interstitial_relative_layout"));
	}
	
	
	//------------------------HugoFun--------------------------------------------//	
	public static WebElement hugoFunBtn() {
		return $(By.xpath("//android.widget.TextView[@text='']"));
	}
	public static WebElement backBtnHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnBack"));
	}
	public static WebElement logoHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/imgLogo"));
	}
	public static WebElement ticketsBtnHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnTichets"));
	}
	public static WebElement bioBtnHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnBio"));
	}
	public static WebElement greetingsTxtHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/txtName"));
	}
	public static WebElement searchTxtHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/contEditSearch"));
	}
	public static WebElement categoryHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='" + Category + "']"));
	}
	public static WebElement imgCategoryHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/imgCat"));
	}
	public static WebElement DescoveryBtnHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/conteinerDate"));
	}
	public static WebElement searchOnTextHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/editSearch"));
	}
	public static WebElement selectResultHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='"+Event+"']"));	
	}
	public static WebElement backBtnOnEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/imageView2"));	
	}
	public static WebElement shareBtnOnEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/imageView3"));	
	}
	public static WebElement availabilityOnEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnAvailability"));	
	}
	public static WebElement datesOnEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnDates"));	
	}
	public static WebElement dateBoxHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/date"));	
	}
	public static WebElement dateBoxSwipeHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/recyclerDate"));	
	}
	public static WebElement moreTktHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnMas"));	
	}
	public static WebElement lessTktHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnMenos"));	
	}
	public static WebElement nextHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnNext"));	
	}
	public static WebElement addCardHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnAddCard"));	
	}
	public static WebElement swipeCardHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/type"));	
	}
	public static WebElement selectCardHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='"+Last4NumberCard+"']"));	
		}
	public static WebElement promotionalCodeHugoFun() {
	return $(By.xpath("//android.widget.Button[@text='+ C�DIGO PROMOCIONAL']"));	
	}
	public static WebElement promoCodeHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/promoCode"));	
	}
	public static WebElement okBtnPromoHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnOK"));	
	}
	public static WebElement payFunHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnPayFun"));	
	}
	public static WebElement exitBtnHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnExit"));	
	}
	public static WebElement selectTicketHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='Caifanes']"));	
	}
	public static WebElement TicketHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/tvTitle"));		
	}
	public static WebElement helpEventHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='�Necesitas Ayuda?']"));			
	}
	public static WebElement infoExpEventHugoFun() {
		return $(By.xpath("//android.widget.TextView[@text='Informaci�n de la experiencia']"));			
	}
	public static WebElement streamingEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnStreaming"));		
	}
	public static WebElement transferEventHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnTransfer"));		
	}
	public static WebElement contactHugoFun() {
		return $(By.id("com.hugoapp.client.dev:id/btnContact"));		
	}
	
	//---------------------------------------------------------------------------//	
	
	
	//------------------------Prime--------------------------------------------//	
	
	public static WebElement primeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/lavPrime"));
	}
	public static WebElement primeCardBenefit() {
		return $(By.id("com.hugoapp.client.dev:id/ivBenefit"));
	}
	public static WebElement subscribeBtnOnboarding() {
		return $(By.id("com.hugoapp.client.dev:id/btnSubscribe"));
	}
	public static WebElement subscriptionType() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.CheckedTextView[@text='Pago cada minuto de  $0.99']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.CheckedTextView[@text='Charged every minute for $0.99']"));
	 		break;
	 	}
	 	return SelectLanguage;
	}
	public static WebElement okBtnSubscription() {
		return $(By.id("com.hugoapp.client.dev:id/btnOk"));
		}
	public static WebElement checkTermsPrime() {
		return $(By.id("com.hugoapp.client.dev:id/chkTerm"));
		}
	public static WebElement selectAnotherCard() {
		return $(By.id("com.hugoapp.client.dev:id/tvCardNumber"));
		}
	public static WebElement btnAcceptSubscription() {
		return $(By.id("com.hugoapp.client.dev:id/btnAccept"));
		}
	public static WebElement btnFinishSubscription() {
		return $(By.id("com.hugoapp.client.dev:id/btnAccept"));
		}
	public static WebElement subscribeBtnCheckout() {
		return $(By.id("com.hugoapp.client.dev:id/materialButton"));
		}
	
//---------------------------------------------------------------------------//
	
//------------------------Mastersearch--------------------------------------------//	
	
	
	public static WebElement masterSearchText() {
		return $(By.id("com.hugoapp.client.dev:id/textViewOfferServices"));
	}
	public static WebElement searchMasterSearch() {
		return $(By.id("com.hugoapp.client.dev:id/editTextSearch"));
	}
	public static WebElement resultMasterSearch() {
		return $(By.xpath("//android.widget.TextView[@text='" + SearchResult + "']"));
	}
	public static WebElement reloadClickOnSearch() {
		return $(By.id("com.hugoapp.client.dev:id/pgbAddSearch"));
	}
	
	
	
//---------------------------------------------------------------------------//	
	
	// Pendiente el QR Btn ya que no se puede utilizar
	public static WebElement qrBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/imageView8"));
	}

	public static WebElement cashbackBtnHome() {
		//return $(By.id("com.hugoapp.client.dev:id/constraintLayout7"));
		return $(By.id("com.hugoapp.client.dev:id/btnCash"));
	}

	public static WebElement notificationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageViewIconNotifications"));
	}
	
	public static WebElement newuiHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/lytOnlyMessage"));
	}
	public static WebElement newuiHugoPayQrBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btnQr"));
	}

	public static WebElement addressBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/constraintLayoutAddress"));
	}

	public static WebElement servicesBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/servicesFragment"));
	}

	public static WebElement hugoAutoBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/onBoardingActivity"));
	}

	public static WebElement hugoPayBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/hugoPayFragment"));
	}
	public static WebElement pinLoginHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/codePinView"));
	}
	public static WebElement notificationsHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/buttonNotifications"));
	}
	public static WebElement optionsHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/imageMenu"));
	}
	public static WebElement listCardHugoPay1() {
		return $(By.id("com.hugoapp.client.dev:id/viewPagerListCards2"));
	}
	public static WebElement listCardHugoPay2() {
		//return $(By.id("com.hugoapp.client.dev:id/baseLayoutCard"));
		return $(By.id("com.hugoapp.client.dev:id/imageViewLogoCard"));
	}
	public static WebElement nameCardHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/nameCardHugoPay"));
	}
	public static WebElement lastNumberCardHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/textViewLastNumber"));
	}
	public static WebElement amountCashBackHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/TextViewAmountCashBack"));
	}
	public static WebElement topUpHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/ButtonTopup"));
	}
	public static WebElement trackerHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/buttonClientControl"));
	}
	public static WebElement pagosServiciosHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/textServicesPay"));
	}
	public static WebElement allTransactionsHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/viewAllTransactions"));
	}
	public static WebElement qrCodeBtnHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/qrCodeButton"));
	}
	public static WebElement addCardHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/layoutAddCardConfiguration"));
	}
	public static WebElement securityHugoPay() {
		return $(By.xpath("//androidx.recyclerview.widget.RecyclerView//android.widget.LinearLayout[@index='2']"));
		//com.hugoapp.client.dev:id/layoutSecurityConfiguration
	}
	public static WebElement helpHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/layoutHelpConfiguration"));
		//com.hugoapp.client.dev:id/layoutHelpConfiguration
	}
	public static WebElement addCardOnHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/floatingButtonAddCard"));
	}
	public static WebElement changePinHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/layoutChangePin"));
	}
	public static WebElement changePinHugoPaytxt() {
		return $(By.id("com.hugoapp.client.dev:id/codePinView"));
	}
	public static WebElement nextChangePinBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonNextStep"));
	}
	public static WebElement faqsHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/layoutFaq"));
	}
	public static WebElement nameCardHugoPayQr() {
		return $(By.id("com.hugoapp.client.dev:id/nameCardHugoPayQr"));
	}
	public static WebElement rightBtnCards() {
		return $(By.id("com.hugoapp.client.dev:id/buttonRight"));
	}
	public static WebElement addCardOnHugoPayQr() {
		return $(By.id("com.hugoapp.client.dev:id/addCardButton"));
	}
	public static WebElement closeBtnQrHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/closeButton"));
	}
	public static WebElement selectCardHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSelectCard"));
	}
	public static WebElement removeCardOnHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonRemoveCard"));
	}
	public static WebElement editCardOnHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonEditCard"));
	}
	public static WebElement confirmDeleteCardHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	}
	
	public static WebElement orderHistoryBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/trackingHistoryFragment"));
	}

	public static WebElement chatBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/chatActivity"));
	}

	public static WebElement seeAllBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/constraintLayoutSeeAll"));
		// com.hugoapp.client.dev:id/textViewSeeAll
	}

	public static WebElement recycleView1() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServiceToSwipe1 + "']"));
	}

	public static WebElement recycleView2() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServiceToSwipe2 + "']"));
	}

	public static WebElement recycleView3() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServiceToSwipe3 + "']"));
	}

	public static WebElement recycleView4() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServiceToSwipe4 + "']"));
	}

	public static WebElement okBtnMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	}

	public static WebElement backMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/back_btn_shipment"));
	}

	public static WebElement infoMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/info_shipment"));
	}

	public static WebElement centerMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/move_to_my_current_location"));
	}

	public static WebElement pickUpAddressTxt() {
		return $(By.id("com.hugoapp.client.dev:id/pickingAddress"));
	}

	public static WebElement deliveryAddresTxt() {
		return $(By.id("com.hugoapp.client.dev:id/dropOffAddress"));
	}

	public static WebElement pickUpTxt() {
		return $(By.id("com.hugoapp.client.dev:id/input"));
	}

	public static WebElement backPickUpAddressBtn() {
		return $(By.id("com.hugoapp.client.dev:id/back_btn"));
	}

	public static WebElement currentLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/current_location_shipment"));
	}

	public static WebElement backPinBtn() {
		return $(By.id("com.hugoapp.client.dev:id/backButtonPin"));
	}

	public static WebElement inputAddressPinTxt() {
		return $(By.id("com.hugoapp.client.dev:id/inputAddressPin"));
	}

	public static WebElement confirmLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonConfirmLocation"));
	}

	public static WebElement descAddressTxt() {
		return $(By.id("com.hugoapp.client.dev:id/descAddress"));
	}

	public static WebElement descriptionShipmentTxt() {
		return $(By.id("com.hugoapp.client.dev:id/descriptionShipment"));
	}

	public static WebElement insertIMGBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCamera"));
	}

	public static WebElement saveDescriptionBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSaveDescription"));
	}

	public static WebElement specificLocationBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + Address + "']"));
	}

	public static WebElement specificServiceBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + Service + "']"));
	}

	public static WebElement shipmentBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonShipment"));
	}

	public static WebElement titleOnService() {
		return $(By.id("com.hugoapp.client.dev:id/textViewTitle"));
	}

	public static WebElement backBtnOnService() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBack"));
	}

	public static WebElement searchTxtOnService() {
		return $(By.id("com.hugoapp.client.dev:id/textViewSearch"));
	}
	public static WebElement newuisearchTxtOnService() {
		return $(By.id("com.hugoapp.client.dev:id/searchView"));
	}
	

	public static WebElement sortBtnOnService() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonSort"));
	}

	public static WebElement searchElasticOnService() {
		return $(By.id("com.hugoapp.client.dev:id/edtSearchElastic"));
	}

	public static WebElement resultOnSearch() {
		return $(By.xpath("//android.widget.TextView[@text='" + SearchResult + "']"));
	}

	public static WebElement logoScreenMenu() {
		return $(By.id("com.hugoapp.client.dev:id/logo"));
	}

	public static WebElement productSearch() {
		return $(By.xpath("//android.widget.TextView[@text='" + Product + "']"));
	}

	public static WebElement moreProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/text_view_plus"));
	}

	public static WebElement lessProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/text_view_minus"));
	}

	public static WebElement noteOnProductTxt() {
		return $(By.id("com.hugoapp.client.dev:id/note"));
	}

	public static WebElement addToOrderBtn() {
		return $(By.id("com.hugoapp.client.dev:id/addToOrderBtn"));
	}

	public static WebElement searchBtnHugoMarket() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonSearch"));
	}

	public static WebElement scrollOnHugoMarket() {
		return $(By.id("com.hugoapp.client.dev:id/root_browse"));
	}

	public static WebElement titleCategoryOnHugoMarket() {
		return $(By.xpath("//android.widget.TextView[@text='" + CategoryHugoMarket + "']"));
	}

	public static WebElement changeLayoutBtn() {
		return $(By.id("com.hugoapp.client.dev:id/change_layout"));
	}

	public static WebElement addProductOnLayout() {
		return $(By.id("com.hugoapp.client.dev:id/imageViewAddProduct"));
	}

	public static WebElement searchOnServiceHM() {
		return $(By.id("com.hugoapp.client.dev:id/edit_search"));
	}

	public static WebElement clearSearchOnHM() {
		return $(By.id("com.hugoapp.client.dev:id/delete_btn"));
	}

	public static WebElement productOnHM() {
		return $(By.xpath("//android.widget.TextView[@text='" + Product + "']"));
	}

	public static WebElement recargasAmountBtn() {
		return $(By.id("com.hugoapp.client.dev:id/layout_amount"));
	}

	public static WebElement recargasBackBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBackTelephony"));
	}

	public static WebElement editPhoneNumberBtn() {
		return $(By.id("com.hugoapp.client.dev:id/putNewPhone"));
	}

	public static WebElement backEditPhoneNumberBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBackNumberPhone"));
	}

	public static WebElement phoneNumberTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextPhone"));
	}

	public static WebElement saveNewPhoneBtn() {
		return $(By.id("com.hugoapp.client.dev:id/saveNewPhone"));
	}

	public static WebElement operadoraBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + PhoneOperator + "']"));
	}

	public static WebElement loadingRecargasLogo() {
		return $(By.id("com.hugoapp.client.dev:id/includeProgress"));
	}

	public static WebElement continueBtnRecargas() {
		return $(By.id("com.hugoapp.client.dev:id/buttonTopup"));
	}

	public static WebElement btnBarCodePagoServicios() {
		return $(By.id("com.hugoapp.client.dev:id/bar_code_button"));
	}

	public static WebElement serviceOnPagoServicios() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServicePagoAServicio + "']"));
	}

	public static WebElement searchProviderTxt() {
		return $(By.id("com.hugoapp.client.dev:id/search_providers"));
	}

	public static WebElement clickOnResultProvider() {
		return $(By.xpath("//android.widget.TextView[@text='" + Provider + "']"));
	}

	public static WebElement numberContractTxt() {
		return $(By.id("com.hugoapp.client.dev:id/text_code_pay"));
	}

	public static WebElement validateContractNumber() {
		return $(By.id("com.hugoapp.client.dev:id/validate_code_pay"));
	}

	public static WebElement readCodeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/go_to_read_bar_code_2"));
	}

	public static WebElement listOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/layout_type_code_select"));
	}

	public static WebElement clickOnListOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/textView_code_label"));
	}

	public static WebElement cancelBtnListOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCancel"));
	}

	public static WebElement clickTypeOfContract() {
		return $(By.xpath("//android.widget.TextView[@text='" + TypeOfContract + "']"));
	}
	public static WebElement viewOrderBtn() {
		return $(By.id("com.hugoapp.client.dev:id/view_order_btn"));
	}
	public static WebElement productImage() {
		return $(By.id("com.hugoapp.client.dev:id/img_summary"));
	}

	public static WebElement cancelOrderBtn() {
		return $(By.id("com.hugoapp.client.dev:id/textViewCancelOrder"));
	}

	public static WebElement deleteProductBtn() {
		return $(By.id("com.hugoapp.client.dev:id/row_option_btn"));
	}

	public static WebElement confirmDeleteBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btn_positive"));
	}

	public static WebElement cancelDeleteBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btn_negative"));
	}

	public static WebElement addProductBtnOnDraft() {
		return $(By.id("com.hugoapp.client.dev:id/constraintLayoutAddProduct"));
	}

	public static WebElement processOrderBtn() {
		return $(By.id("com.hugoapp.client.dev:id/processOrderBtn"));
	}

	public static WebElement addCardBtnOnProcessOrder() {
		return $(By.id("com.hugoapp.client.dev:id/cc_list"));
	}

	public static WebElement findCardTxt() {
		return $(By.xpath("//android.widget.TextView[@text='" + Name + " " + LastName + "']"));
	}

	public static WebElement findCashTxt() {
		
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Efectivo']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Cash']"));
	 		break;
	 	}
	 	return SelectLanguage;
		//return $(By.xpath("//android.widget.TextView[@text='Cash']"));
	}
	public static WebElement policiesHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/textViewInformation"));
	}
	
	public static WebElement continueBtnPaymentMethod() {
		return $(By.id("com.hugoapp.client.dev:id/buttonPaymentSelected"));
	}

	public static WebElement logoCheckoutImage() {
		return $(By.id("com.hugoapp.client.dev:id/circleImageLogo"));
	}

	public static WebElement promoCodeBtn() {
	/*	switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Aplicar cup�n']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Apply coupon']"));
	 		break;
	 	}
	 	return SelectLanguage;
	 	*/
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Ingresa tu codigo promocional']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Enter your promo code']"));
	 		break;
	 	}
	 	return SelectLanguage;
		
	}

	public static WebElement promoCodeTxt() {
		return $(By.id("com.hugoapp.client.dev:id/textViewCodeDesc"));
	}

	public static WebElement promoReadyBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCodePromoReady"));
	}

	public static WebElement promoCancelBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCodePromoCancel"));
	}

	public static WebElement successfullPromoBtn() {
		return $(By.id("android:id/button1"));
	}

	public static WebElement prizesBtn() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@contains='Tienes premios que']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@contains='You have prizes that can apply for this order']"));
	 		break;
	 	}
		return SelectLanguage;
	}

	public static WebElement freeDeliveryBtn() {
		return $(By.id("com.hugoapp.client.dev:id/title_prize"));
	}

	public static WebElement selectAwardBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btn_select_prize"));
	}

	public static WebElement seeAllBtn() {
		return $(By.id("com.hugoapp.client.dev:id/textViewSeeAll"));
	}

	public static WebElement immediateDeliveryBtn() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Entrega inmediata']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Immediate delivery']"));
	 		break;
	 	}
	 	return SelectLanguage;
		
	}

	public static WebElement scheduleOrderBtn() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Programar orden']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Schedule order']"));
	 		break;
	 	}
	 	return SelectLanguage;
		
	}
	
	public static WebElement btnAgreeScheduleOrder() {
		return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	}
	

	public static WebElement instructionsOrder() {
		return $(By.id("com.hugoapp.client.dev:id/et_address"));
	}

	public static WebElement paymentMethodBtn() {
		return $(By.id("com.hugoapp.client.dev:id/textViewMethod"));
	}

	public static WebElement processOrderStep1() {
		return $(By.xpath("//android.widget.Button"));
	}

	public static WebElement cvcConfirmOrder() {
		return $(By.id("com.hugoapp.client.dev:id/editTextCVC"));
	}

	public static WebElement confirmLocation() {
		return $(By.id("com.hugoapp.client.dev:id/button"));
	}
	
	public static WebElement alertDialog() {
		return $(By.id("android:id/button1"));
	}
	
	public static WebElement goToTrackingBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSecond"));
	}
	
	public static WebElement addCardInScheduleOrder() {
		return $(By.id("com.hugoapp.client.dev:id/imageViewAddCard"));
	}

	public static WebElement backTrackingBtn() {
		return $(By.id("com.hugoapp.client.dev:id/back_btn"));
	}

	public static WebElement ratingPartner() {
		return $(By.id("com.hugoapp.client.dev:id/ratingBar_partner"));
	}

	public static WebElement ratingDriver() {
		return $(By.id("com.hugoapp.client.dev:id/ratingBar_driver"));
	}

	public static WebElement ratingHugo() {
		return $(By.id("com.hugoapp.client.dev:id/ratingBar_hugo"));
	}

	public static WebElement tipDriver() {
		return $(By.id("com.hugoapp.client.dev:id/tip_driver"));
	}

	public static WebElement tip1Driver() {
		return $(By.id("com.hugoapp.client.dev:id/rbtn_1"));
	}

	public static WebElement tip2Driver() {
		return $(By.id("com.hugoapp.client.dev:id/rbtn_2"));
	}

	public static WebElement tip3Driver() {
		return $(By.id("com.hugoapp.client.dev:id/rbtn_3"));
	}

	public static WebElement tipChangeCardBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageCard"));
	}

	public static WebElement thanksBtnTip() {
		return $(By.id("com.hugoapp.client.dev:id/button_thanks"));
	}

	public static WebElement sendRatingBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btn_send"));
	}

	public static WebElement skipRaitingBtn() {
		return $(By.id("com.hugoapp.client.dev:id/no_rating"));
	}

	public static WebElement cardHolderTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextCardHolder"));
	}

	public static WebElement cardNumberTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextCardNumber"));
	}

	public static WebElement dueDateTxt() {
		return $(By.id("com.hugoapp.client.dev:id/flTextExpiredDate"));
	}

	public static WebElement identifierTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextIdentifier"));
	}

	public static WebElement purpleColorRadioBtn() {
		return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorPurple"));
	}

	public static WebElement pinkColorRadioBtn() {
		return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorPink"));
	}

	public static WebElement whiteColorRadioBtn() {
		return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorWhite"));
	}

	public static WebElement yellowColorRadioBtn() {
		return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorYellow"));
	}

	public static WebElement orangeColorRadioBtn() {
		return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorOrange"));
	}

	public static WebElement saveCardBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSaveCard"));
	}

	public static WebElement cvcPinTxt() {
		return $(By.id("com.hugoapp.client.dev:id/cvcPinView"));
	}

	public static WebElement cvcPinPaymentTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextCVC"));
	}

	public static WebElement confirmSaveCardBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	}
	public static WebElement invoiceBtn() {
		switch(Language) {
	 	case "es":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Enviarme factura']"));
	 		break;
	 	case "en":
	 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Do you need an invoice?']"));
	 		break;
	 	}
	 	return SelectLanguage;
		//Send me invoice
	}
	public static WebElement searchInvoice() {
		return $(By.xpath("//android.widget.TextView[@text='" + Name +"']"));
	}
	public static WebElement addInvoiceBtn() {
		//return $(By.id("com.hugoapp.client.dev:id/floatingButtonAdd"));
		return $(By.xpath("//android.widget.FrameLayout//android.view.ViewGroup//android.view.ViewGroup//android.widget.ImageView[@index='1']"));

	}
	public static WebElement nameInvoiceTxt() {
		//return $(By.id("com.hugoapp.client.dev:id/editTextName"));
		return $(By.xpath("//android.widget.EditText[@text='Juan Carlos Alda']"));
	}
	public static WebElement nitInvoiceTxt() {
		//return $(By.id("com.hugoapp.client.dev:id/editTextNit"));
		return $(By.xpath("//android.widget.EditText[@text='0000 000000 000 0']"));
	}
	public static WebElement addressInvoiceTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextAddress"));
	}
	public static WebElement emailInvoiceTxt() {
		//return $(By.id("com.hugoapp.client.dev:id/editTextEmail"));
		return $(By.xpath("//android.widget.EditText[@text='Email']"));
	}
	public static WebElement saveInvoiceBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonAction"));
	}
	public static WebElement selectInvoiceBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + Name +"']"));
	}
	public static WebElement useInvoiceBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonUseInfo"));
	}
	public static WebElement useInvoiceMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonBillSelect"));
	}
	public static WebElement deleteInvoiceBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonRemove"));
	}
	public static WebElement instructionsOnCheckoutTxt() {
		return $(By.id("com.hugoapp.client.dev:id/et_address"));
	}
	public static WebElement errorTxt() {
		return $(By.xpath("//android.widget.TextView[@text='There was a problem, try again later']"));
	}
	public static WebElement changeTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextField"));
	}
	public static WebElement dontNeedChangeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonNotNeedChange"));
	}
	public static WebElement readyChangeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonReady"));
	}
	public static WebElement amountCarrusel() {
		return $(By.xpath("//android.widget.TextView[@text='3']"));
	}
	public static WebElement providerLogoIMG() {
		return $(By.id("com.hugoapp.client.dev:id/textView_name_provider_checkout"));
	}
	public static WebElement editPhoneNumberCheckout() {
		return $(By.id("com.hugoapp.client.dev:id/textViewEdit"));
	}
	public static WebElement selectAnotherCardCheckout() {
		return $(By.id("com.hugoapp.client.dev:id/textView_card"));
	}
	public static WebElement chooseThisCard() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSelectCard"));
	}
	public static WebElement rechargeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/pay_service"));
	}
	public static WebElement continueBtnRecharge() {
		return $(By.id("com.hugoapp.client.dev:id/button_dialog_box"));
	}
	public static WebElement seeDetailBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btnTracking"));
	}
	public static WebElement makeAnotherTopUp() {
		return $(By.id("com.hugoapp.client.dev:id/btnRequest"));
	}
	public static WebElement makeAnotherRefil() {
		return $(By.id("com.hugoapp.client.dev:id/newPayService"));
	}
	public static WebElement acceptSimilarProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonAcceptSimilarProducts"));
	}
	public static WebElement doNotAcceptSimilarProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonNoAcceptSimilarProducts"));
	}
	public static WebElement alertMinimumPurchase() {
		return $(By.id("com.hugoapp.client.dev:id/alertTitle"));
	}
	public static WebElement acceptAlertMinimumPurchase() {
		return $(By.id("android:id/button1"));
	}
	public static WebElement closeDialogSimilarProduct() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCloseDialog"));
	}
	public static WebElement addMoreOfTheSameProduct() {
		return $(By.id("com.hugoapp.client.dev:id/qty"));
	}
	public static WebElement paymentContainer() {
		return $(By.id("com.hugoapp.client.dev:id/cc_content"));
	}
	public static WebElement cashPaymentMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/cashOption"));
	}
	public static WebElement addInvoiceMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/buttonAddBillData"));
	}
	public static WebElement payInPickUpLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/optionPickingPlace"));
	}
	public static WebElement payInDeliveryLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/optionDropOffPlace"));
	}
	public static WebElement sendShipmentFinalStepBtn() {
		return $(By.id("com.hugoapp.client.dev:id/sendShipment"));
	}
	public static WebElement changeTxtMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/inputChange"));
	}
	public static WebElement readyBtnMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/buttonReady"));
	}
	public static WebElement changeNotNeedBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonNotNeed"));
	}
	public static WebElement totalRideRaitingBtn() {
		return $(By.id("com.hugoapp.client.dev:id/totalRide"));
	}
	public static WebElement raitingLvl1Mandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/rating_1"));
	}
	public static WebElement raitingLvl2Mandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/rating_2"));
	}
	public static WebElement raitingLvl3Mandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/rating_3"));
	}
	public static WebElement raitingLvl4Mandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/rating_4"));
	}
	public static WebElement continueRaiting() {
		return $(By.id("com.hugoapp.client.dev:id/continue_rating"));
	}
	public static WebElement commentPositiveTxt() {
		return $(By.id("com.hugoapp.client.dev:id/commentPositive"));
	}
	public static WebElement tip1Btn() {
		return $(By.id("com.hugoapp.client.dev:id/tip_1"));
	}
	public static WebElement tip2Btn() {
		return $(By.id("com.hugoapp.client.dev:id/tip_2"));
	}
	public static WebElement tip3Btn() {
		return $(By.id("com.hugoapp.client.dev:id/tip_3"));
	}
	public static WebElement tipPlusBtn() {
		return $(By.id("com.hugoapp.client.dev:id/more_button"));
	}
	public static WebElement rewardDriverMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/reward"));
	}
	public static WebElement DoNotRewardMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/no_reward"));
	}
	public static WebElement qtyPlusTxt() {
		return $(By.id("com.hugoapp.client.dev:id/doc_number"));
	}
	public static WebElement readyPlusBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonDocReady"));
	}
	public static WebElement sendRaitingMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/thanks_improve"));
	}
	public static WebElement usePin() {
		return $(By.xpath("//android.widget.TextView[@text='USE PIN']"));
	}

}
