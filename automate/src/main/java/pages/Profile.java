package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;



public class Profile {
	public static String Name  = System.getProperty("Name");
	public static String LastName  = System.getProperty("LastName");
	public static String SearchAddress  = System.getProperty("SearchAddress");
	public static String Language = System.getProperty("language");
	public static WebElement SelectLanguage;
	
	 public static WebElement profileBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/circleImageViewUserPhoto"));
	    }
	 public static WebElement profileImageView()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/imageViewProfile"));
	    }
	 public static WebElement editProfileBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/imageButtonEdit"));
	    }
	 public static WebElement editImageProfileBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/circleImageEdit"));
	    }
	 public static WebElement galleryBtn()
	    {
			 switch(Language) {
			 	case "es":
			 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Galer�a']"));
			 		break;
			 	case "en":
			 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Gallery']"));
			 		break;
			 	
			 }
		 	return SelectLanguage;
	    }
	 public static WebElement pictureBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Im�genes']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Pictures']"));
		 		break;
		 	}
		 	return SelectLanguage;
	       
	    }
	 public static WebElement selectImageGallery()
	    {
	        return $(By.id("com.sec.android.gallery3d:id/thumbnail"));
	    }
	 public static WebElement readyBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonCropReady"));
	    }
	 public static WebElement fullNameProfileTxt()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/editTextName"));
	    }
	 public static WebElement emailProfileTxt()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/editTextEmail"));
	    }
	 
	 public static WebElement updateProfileBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/buttonUpdateProfile"));
	    }
	 
	 public static WebElement prizesBtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/textViewPrize"));
	    }
	 public static WebElement addressBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Tus direcciones']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Your addresses']"));
		 		break;
		 	}
		 	return SelectLanguage;
	        
	    }
	 public static WebElement paymentsBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Formas de pago']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Payment methods']"));
		 		break;
		 	}
		 	return SelectLanguage;
		 
	    }
	 public static WebElement preferencesBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Preferencias']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Preferences']"));
		 		break;
		 	}
		 	return SelectLanguage;
		 
	    }
	 public static WebElement helpBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Ayuda']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Help']"));
		 		break;
		 	}
		 	return SelectLanguage;
		 
	    }
	 public static WebElement logOutBtn()
	    {
		 switch(Language) {
		 	case "es":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Cerrar Sesi�n']"));
		 		break;
		 	case "en":
		 		SelectLanguage = $(By.xpath("//android.widget.TextView[@text='Log out']"));
		 		break;
		 	}
		 	return SelectLanguage;
		 
	    }
	 public static WebElement yesLogOutBtn()
	    {
		 return $(By.id("android:id/button1"));
	    }

	 public static WebElement nologOutBtn()
	    {
		 return $(By.id("android:id/button2"));
	    }

	 public static WebElement btnAddAddress()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/add_btn"));
	    }
	 public static WebElement confirmLocationBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonConfirmLocation"));
	    }
	 public static WebElement numberHouseTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/editTextNumHouse"));
	    }
	 public static WebElement saveAddressBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonSaveAddress"));
	    }
	 public static WebElement confirmSaveAddressBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/button_dialog_box"));
	    }
	 public static WebElement searchAddress()
	    {
		 return $(By.xpath("//android.widget.TextView[@text='" + SearchAddress + "']"));
	    }
	 public static WebElement deleteAddressBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/remove_btn"));
	    }
	 public static WebElement confirmDeleteBtn()
	    {
		 return $(By.id("android:id/button1"));
	    }
	 public static WebElement backAddressBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/back_btn"));
	    }
	 public static WebElement addCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/floatingButtonAddCard"));
	    }
	 public static WebElement cardHolderTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/editTextCardHolder"));
	    }
	 public static WebElement cardNumberTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/editTextCardNumber"));
	    }
	 public static WebElement dueDateTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/flTextExpiredDate"));
	    }
	 public static WebElement identifierTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/editTextIdentifier"));
	    }
	 public static WebElement purpleColorRadioBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorPurple"));
	    }
	 public static WebElement pinkColorRadioBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorPink"));
	    }
	 public static WebElement whiteColorRadioBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorWhite"));
	    }
	 public static WebElement yellowColorRadioBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorYellow"));
	    }
	 public static WebElement orangeColorRadioBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/radioButtonPickColorOrange"));
	    }
	 public static WebElement saveCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonSaveCard"));
	    }
	 public static WebElement cvcPinTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/cvcPinView"));
	    }
	 public static WebElement confirmSaveCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	    }
	 public static WebElement findCardTxt()
	    {
		 return $(By.xpath("//android.widget.TextView[@text='"+Name+" "+LastName+"']"));
	    }
	 public static WebElement removeCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/imageButtonRemoveCard"));
	    }
	 public static WebElement editCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/imageButtonEditCard"));
	    }
	 public static WebElement selectCardBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonSelectCard"));
	    }
	 public static WebElement backPaymentsBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/imageButtonBack"));
	    }
	 public static WebElement containerStepsProfile()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/linearStepContainer"));
	    }
	 public static WebElement completeProfileBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/buttonUpdateProfile"));
	    }
	 
	 //Completar el perfil en ambientes de prueba no funciona
	 
	 public static WebElement changeMyPasswordBtn()
	    {
		 return $(By.xpath("//android.widget.TextView[@text='Change Password']"));
	    }
		
	 public static WebElement changePasswordTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/textinput_placeholder"));
	    }
	 public static WebElement confirmChangePasswordTxt()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/edtConfirmPass"));
	    }
	 public static WebElement savePasswordBtn()
	    {
		 return $(By.id("com.hugoapp.client.dev:id/btnPassDoneSave"));
	    }
	 public static WebElement permisionDialogBrowserstack()
	    {
		 return $(By.id("com.sec.android.gallery3d:id/button1"));
	    }
	 
}
