package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.codeborne.selenide.Condition;

public class LogIn {
	
	 public static WebElement LoginScreen()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/imageView6")).shouldBe(Condition.visible);
	    }
	 public static WebElement AreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtCodeArea"));
	    }
	 public static WebElement SearchAreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/editSearchCode"));
	    }
	 public static WebElement ResultAreaCode()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/nameCountry"));
	    }
	 public static WebElement TxtPhoneNumber()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtNumberPhone"));
	    }
	 
	 public static WebElement BtnNextLogin()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnNextLogin"));
	    }
	 
	 public static WebElement BtnConfirmPhoneNumber()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnConfirmDialog"));
	    }
	 public static WebElement TxtPassword()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/edtPass"));
	    }
	 public static WebElement BtnShowPassword()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/text_input_end_icon"));
	    }
	 public static WebElement BtnForgotPassword()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/txtLostPass"));
	    }
	 public static WebElement BtnNext()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnPassDone"));
	    }

	 public static WebElement backHeaderbtn()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/btnBackHeaader"));
	    }
	 public static WebElement smsCodeLayout()
	    {
	        return $(By.id("com.hugoapp.client.dev:id/lytCode"));
	    }
	 public static WebElement sendNewCodeBtn()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/txtGetNewCode"));
	    }
	 public static WebElement requestHelpBtn()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/txtNeedHelp"));
	    }
	 public static WebElement txtPasswordSMS()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/textinput_placeholder"));
	    }
	 public static WebElement showPasswordSMSBtn()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/text_input_end_icon"));
	    }

		public static WebElement txtconfirmPasswordSMS()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/edtConfirmPass"));
	    }
	 public static WebElement btnSavePassword()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/btnPassDoneSave"));
	    }
	 public static WebElement greetingsScreen()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/textViewGreeting")).shouldBe(Condition.visible);
	    }
	 public static WebElement createAccounBtn()
	    { 
	        return $(By.id("com.hugoapp.client.dev:id/btnOneSheet"));
	    }

}
