package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class SplashArt {
	
	public static WebElement SignUpScreen()
    {
		return $(By.id("com.hugoapp.client.dev:id/imgWelcome"));

	}

	public static WebElement BtnCreateUser() {
		return $(By.id("com.hugoapp.client.dev:id/btnCreate"));
	}

	public static WebElement BtnLogIn() {
		return $(By.id("com.hugoapp.client.dev:id/btnSesion"));
	}

	public static WebElement BtnGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/txtExplorer"));
	}
	
}
