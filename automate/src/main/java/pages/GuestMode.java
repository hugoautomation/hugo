package pages;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class GuestMode {

	public static String Service = System.getProperty("Service");
	public static String Address = System.getProperty("Address");
	public static String SearchResult = System.getProperty("SearchResult");
	public static String Product = System.getProperty("Product");
	public static String CategoryHugoMarket = System.getProperty("CategoryHugoMarket");
	public static String PhoneOperator = System.getProperty("PhoneOperator");
	public static String ServicePagoAServicio  = System.getProperty("ServicePagoAServicio");
	public static String Provider  = System.getProperty("Provider");
	public static String TypeOfContract  = System.getProperty("TypeOfContract");
	public static String Language = System.getProperty("language");
	
	public static WebElement onboardingGuestModeScreen() {
		return $(By.id("com.hugoapp.client.dev:id/imgSlide"));
	}

	public static WebElement backBtnOnboarding() {
		return $(By.id("com.hugoapp.client.dev:id/btnBackOnb"));
	}

	public static WebElement skipBtnOnboarding() {
		return $(By.id("com.hugoapp.client.dev:id/btnSkip"));
	}

	public static WebElement swipeImgOnboarding() {
		return $(By.id("com.hugoapp.client.dev:id/masterwelcome"));
	}

	public static WebElement continueBtnOnboarding() {
		return $(By.id("com.hugoapp.client.dev:id/btnDoneOnb"));
	}

	public static WebElement profileBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/circleImageViewUserPhoto"));
	}

	// Pendiente el QR Btn ya que no se puede utilizar
	public static WebElement qrBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/imageView8"));
	}

	public static WebElement cashbackBtnHome() {
		//return $(By.id("com.hugoapp.client.dev:id/constraintLayout7"));
		return $(By.id("com.hugoapp.client.dev:id/btnCash"));
	}

	public static WebElement addressBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/constraintLayoutAddress"));
	}

	public static WebElement servicesBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/servicesFragment"));
	}

	public static WebElement hugoAutoBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/onBoardingActivity"));
	}

	public static WebElement hugoPayBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/hugoPayFragment"));
	}

	public static WebElement orderHistoryBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/trackingHistoryFragment"));
	}

	public static WebElement chatBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/chatActivity"));
	}

	public static WebElement seeAllBtnHome() {
		return $(By.id("com.hugoapp.client.dev:id/constraintLayoutSeeAll"));
		// com.hugoapp.client.dev:id/textViewSeeAll
	}

	public static WebElement backBtnProfileGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/btnBackWelcome"));
	}

	public static WebElement createBtnProfileGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/btnCreate"));
	}

	public static WebElement signInBtnProfileGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/btnSesion"));
	}

	public static WebElement onboardingHugoPayGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/imageOnBoarding"));
	}

	public static WebElement termsConditionsHugoPayGuestMode() {
		return $(By.id("com.hugoapp.client.dev:id/termsConditionsHugoPay"));
	}

	public static WebElement moreInfoBtnHugoPay() {
		return $(By.id("com.hugoapp.client.dev:id/buttonMoreInformation"));
	}

	public static WebElement createAccountHugoPayBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonMoreInformation"));
	}

	public static WebElement createAccountBtn() {
		return $(By.id("com.hugoapp.client.dev:id/btnOneSheet"));
	}

	public static WebElement backOnboardingHugoPayBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBack"));
	}

	public static WebElement recycleView() {
		return $(By.xpath("//androidx.recyclerview.widget.RecyclerView"));
	}

	public static WebElement okBtnMandaditos() {
		return $(By.id("com.hugoapp.client.dev:id/buttonOk"));
	}

	public static WebElement backMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/back_btn_shipment"));
	}

	public static WebElement infoMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/info_shipment"));
	}

	public static WebElement centerMandaditosBtn() {
		return $(By.id("com.hugoapp.client.dev:id/move_to_my_current_location"));
	}

	public static WebElement pickUpAddressTxt() {
		return $(By.id("com.hugoapp.client.dev:id/pickingAddress"));
	}

	public static WebElement deliveryAddresTxt() {
		return $(By.id("com.hugoapp.client.dev:id/dropOffAddress"));
	}

	public static WebElement pickUpTxt() {
		return $(By.id("com.hugoapp.client.dev:id/input"));
	}

	public static WebElement backPickUpAddressBtn() {
		return $(By.id("com.hugoapp.client.dev:id/back_btn"));
	}

	public static WebElement currentLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/current_location_shipment"));
	}

	public static WebElement backPinBtn() {
		return $(By.id("com.hugoapp.client.dev:id/backButtonPin"));
	}

	public static WebElement inputAddressPinTxt() {
		return $(By.id("com.hugoapp.client.dev:id/inputAddressPin"));
	}

	public static WebElement confirmLocationBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonConfirmLocation"));
	}

	public static WebElement descAddressTxt() {
		return $(By.id("com.hugoapp.client.dev:id/descAddress"));
	}

	public static WebElement descriptionShipmentTxt() {
		return $(By.id("com.hugoapp.client.dev:id/descriptionShipment"));
	}

	public static WebElement insertIMGBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCamera"));
	}

	public static WebElement saveDescriptionBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonSaveDescription"));
	}

	public static WebElement specificLocationBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + Address + "']"));
	}

	public static WebElement specificServiceBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + Service + "']"));
	}

	public static WebElement shipmentBtn() {
		return $(By.id("com.hugoapp.client.dev:id/buttonShipment"));
	}

	public static WebElement titleOnService() {
		return $(By.id("com.hugoapp.client.dev:id/textViewTitle"));
	}

	public static WebElement backBtnOnService() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBack"));
	}

	public static WebElement searchTxtOnService() {
		return $(By.id("com.hugoapp.client.dev:id/textViewSearch"));
	}

	public static WebElement sortBtnOnService() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonSort"));
	}

	public static WebElement searchElasticOnService() {
		return $(By.id("com.hugoapp.client.dev:id/edtSearchElastic"));
	}

	public static WebElement resultOnSearch() {
		return $(By.xpath("//android.widget.TextView[@text='" + SearchResult + "']"));
	}

	public static WebElement logoScreenMenu() {
		return $(By.id("com.hugoapp.client.dev:id/logo"));
	}

	public static WebElement productSearch() {
		return $(By.xpath("//android.widget.TextView[@text='" + Product + "']"));
	}

	public static WebElement moreProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/text_view_plus"));
	}

	public static WebElement lessProductsBtn() {
		return $(By.id("com.hugoapp.client.dev:id/text_view_minus"));
	}

	public static WebElement noteOnProductTxt() {
		return $(By.id("com.hugoapp.client.dev:id/note"));
	}

	public static WebElement addToOrderBtn() {
		return $(By.id("com.hugoapp.client.dev:id/addToOrderBtn"));
	}

	public static WebElement searchBtnHugoMarket() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonSearch"));
	}

	public static WebElement scrollOnHugoMarket() {
		return $(By.id("com.hugoapp.client.dev:id/root_browse"));
	}

	public static WebElement titleCategoryOnHugoMarket() {
		return $(By.xpath("//android.widget.TextView[@text='" + CategoryHugoMarket + "']"));
	}

	public static WebElement changeLayoutBtn() {
		return $(By.id("com.hugoapp.client.dev:id/change_layout"));
	}

	public static WebElement addProductOnLayout() {
		return $(By.id("com.hugoapp.client.dev:id/imageViewAddProduct"));
	}

	public static WebElement searchOnServiceHM() {
		return $(By.id("com.hugoapp.client.dev:id/edit_search"));
	}

	public static WebElement clearSearchOnHM() {
		return $(By.id("com.hugoapp.client.dev:id/delete_btn"));
	}

	public static WebElement productOnHM() {
		return $(By.xpath("//android.widget.TextView[@text='" + Product + "']"));
	}
	public static WebElement recargasAmountBtn() {
		return $(By.id("com.hugoapp.client.dev:id/layout_amount"));
	}
	public static WebElement recargasBackBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBackTelephony"));
	}
	public static WebElement editPhoneNumberBtn() {
		return $(By.id("com.hugoapp.client.dev:id/putNewPhone"));
	}
	public static WebElement backEditPhoneNumberBtn() {
		return $(By.id("com.hugoapp.client.dev:id/imageButtonBackNumberPhone"));
	}
	public static WebElement phoneNumberTxt() {
		return $(By.id("com.hugoapp.client.dev:id/editTextPhone"));
	}
	public static WebElement saveNewPhoneBtn() {
		return $(By.id("com.hugoapp.client.dev:id/saveNewPhone"));
	}
	public static WebElement operadoraBtn() {
		return $(By.xpath("//android.widget.TextView[@text='" + PhoneOperator + "']"));
	}
	public static WebElement loadingRecargasLogo() {
		return $(By.id("com.hugoapp.client.dev:id/includeProgress"));
	}
	public static WebElement continueBtnRecargas() {
		return $(By.id("com.hugoapp.client.dev:id/buttonTopup"));
	}
	public static WebElement btnBarCodePagoServicios() {
		return $(By.id("com.hugoapp.client.dev:id/bar_code_button"));
	}
	public static WebElement serviceOnPagoServicios() {
		return $(By.xpath("//android.widget.TextView[@text='" + ServicePagoAServicio + "']"));
	}
	public static WebElement searchProviderTxt() {
		return $(By.id("com.hugoapp.client.dev:id/search_providers"));
	}
	public static WebElement clickOnResultProvider() {
		return $(By.xpath("//android.widget.TextView[@text='" + Provider + "']"));
	}
	public static WebElement numberContractTxt() {
		return $(By.id("com.hugoapp.client.dev:id/text_code_pay"));
	}
	public static WebElement validateContractNumber() {
		return $(By.id("com.hugoapp.client.dev:id/validate_code_pay"));
	}
	public static WebElement readCodeBtn() {
		return $(By.id("com.hugoapp.client.dev:id/go_to_read_bar_code_2"));
	}
	public static WebElement listOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/layout_type_code_select"));
	}
	public static WebElement clickOnListOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/textView_code_label"));
	}
	public static WebElement cancelBtnListOfContracts() {
		return $(By.id("com.hugoapp.client.dev:id/buttonCancel"));
	}
	public static WebElement clickTypeOfContract() {
		return $(By.xpath("//android.widget.TextView[@text='" + TypeOfContract + "']"));
	}
}
