package driver;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoIterable;

public class MongoDbDriver {
	
	public void ConnectMongoDB() {	
		try {
		String dbURI = "mongodb://performance:performance@localhost:27017/admin?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&authSource=payment&authMechanism=SCRAM-SHA-1&3t.uriVersion=3&3t.connection.name=Openshift+-+Performance&3t.defaultColor=60,143,255&3t.ssh=true&3t.sshAddress=okd-mongo.hugoapp.com&3t.sshPort=22&3t.sshAuthMode=privateKey&3t.sshUser=developer&3t.sshPKPath=C:\\Users\\Miguel Miranda\\Desktop\\PrivateKey\\Private&3t.sshPKPassphrase=TapTapDelivery&3t.databases=admin,payment&3t.alwaysShowAuthDB=true&3t.alwaysShowDBFromUserRole=true";
		MongoClient mongoClient = new MongoClient(new MongoClientURI(dbURI));
		MongoIterable<String> dbNames = mongoClient.listDatabaseNames();
		for(String dbName: dbNames) {
			System.out.println(dbName);
		}
		}
		catch (Exception e){
			System.out.println(e);
		}
	
}
}
