package driver;

import static configuration.Config.CONFIG;
import static configuration.ProjectProperties.DEVICE_PLATFORM;
import static configuration.ProjectProperties.DEVICE_PLATFORM2;
import static configuration.ProjectProperties.DEVICE_PLATFORM3;
import static configuration.ProjectProperties.HUB;
import static configuration.ProjectProperties.USER_DIR;
import static driver.AppiumServer.APPIUM_SERVICE;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public enum DriverManager
{
    DRIVER;
    private final ThreadLocal<AppiumDriver<MobileElement>> mobileDriver = new ThreadLocal<>();
    private final String DESIRED_CAPABILITIES_PATH_DIR = USER_DIR
            .concat("/src/main/resources/desired_capabilities/");
    private static Logger LOGGER = Logger.getLogger("InfoLogging");

    private String getHubURL() {
        return APPIUM_SERVICE.getUrl();
    }

    private DesiredCapabilities getLocalDesiredCapabilities(String fileName) {
    	 LOGGER.info("file name: "+fileName);
    	 
    	return CONFIG.computeDesiredCaps(fileName);
       
        
    }

    private void setAndroidDriver(String remoteAddress, DesiredCapabilities capabilities) {
        try {
            mobileDriver.set(new AndroidDriver<>(new URL(remoteAddress), capabilities));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
    private void setiOSDriver(String remoteAddress, DesiredCapabilities capabilities) {
        try {
        	 mobileDriver.set(new IOSDriver<>(new URL(remoteAddress), capabilities));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
    private void setBrowserstackDriver(String remoteAddress, DesiredCapabilities capabilities) {
        try {
        	 mobileDriver.set(new AndroidDriver<>(new URL("http://hub.browserstack.com/wd/hub"), capabilities));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    

    public boolean isAndroidPlatform() {

        return DEVICE_PLATFORM.contentEquals("android");
    }
    public boolean isIosPlatform() {

        return DEVICE_PLATFORM2.contentEquals("ios");     
    }
    public boolean isBrowserStackPlatform() {

        return DEVICE_PLATFORM3.contentEquals("browserstack");     
    }

    private String getDesiredCapabilitiesPath() {
        return DESIRED_CAPABILITIES_PATH_DIR
                .concat(HUB)
                .concat("_")
                .concat(DEVICE_PLATFORM)
                .concat(".properties");

       
    }
    
    private String getDesiredCapabilitiesPath2() {
        return DESIRED_CAPABILITIES_PATH_DIR
                .concat(HUB)
                .concat("_")
                .concat(DEVICE_PLATFORM2)
                .concat(".properties");

    }
    private String getDesiredCapabilitiesPath3() {
        return DESIRED_CAPABILITIES_PATH_DIR
                .concat(HUB)
                .concat("_")
                .concat(DEVICE_PLATFORM3)
                .concat(".properties");

    }

    private DesiredCapabilities getDesiredCapabilities()
    {
        return getLocalDesiredCapabilities(getDesiredCapabilitiesPath());
    }
    
    private DesiredCapabilities getDesiredCapabilities2()
    {
        return getLocalDesiredCapabilities(getDesiredCapabilitiesPath2());
    }

    private DesiredCapabilities getDesiredCapabilities3()
    {
        return getLocalDesiredCapabilities(getDesiredCapabilitiesPath3());
    }
    public void setDriver() {
        setAndroidDriver(getHubURL(), getDesiredCapabilities());
       // LOGGER.info("APP: ".concat((String) getDriver().getCapabilities().getCapability("app")));

    }
    
    public void setDriver2() {
    	setiOSDriver(getHubURL(), getDesiredCapabilities2());
      //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

    }
    
    public void setDriver3() {
    	setBrowserstackDriver(getHubURL(), getDesiredCapabilities3());
      //  LOGGER.info("APP: ".concat((String) getDriver2().getCapabilities().getCapability("app")));

    }
    public AppiumDriver<MobileElement> getDriver() {
        return mobileDriver.get();
    }
    
 /*   public IOSDriver<MobileElement> getDriver2() {
        return (IOSDriver<MobileElement>) mobileDriver.get();
    }*/

    public void closeDriver() {
        getDriver().quit();
    //    getDriver2().quit();
    }
}
