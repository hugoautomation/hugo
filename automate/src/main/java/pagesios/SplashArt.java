package pagesios;

import static com.codeborne.selenide.Selenide.$;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class SplashArt {
	
	public static WebElement SignUpScreen()
    {
		return $(By.id("com.hugoapp.client.dev:id/imgWelcome"));
        
    }
	
	public static WebElement BtnCreateUser()
    {
        return $(By.xpath("//XCUIElementTypeStaticText[@name=\"REGÃ�STRATE\"]"));
    }
	public static WebElement BtnLogIn()
    {
        return $(By.xpath("//XCUIElementTypeStaticText[@name=\"INICIAR SESIÃ“N\"]"));
    }
	public static WebElement BtnGuestMode()
    {
        return $(By.xpath("//XCUIElementTypeStaticText[@name=\"Explora como invitado\"]"));
    }

}
