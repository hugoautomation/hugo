import static util.ScreenUtils.copyFile;

import java.io.File;

import org.testng.annotations.Test;

public class PropHugoCash {
	@Test()
	public void ChangeFiles()
	{
		deleteFile();
		replacePaths();
		renameFile();
	}
	
	public void deleteFile() {
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder2 = newpath + "\\src\\main\\resources\\env\\test.properties";
		File file= new File(otherFolder2); 
		file.delete();
	}
	public void renameFile() {
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder1 = newpath + "\\src\\main\\resources\\env\\test3.properties";
		String otherFolder2 = newpath + "\\src\\main\\resources\\env\\test.properties";
		File file = new File(otherFolder1);
	    File rename = new File(otherFolder2);
	    file.renameTo(rename);
	}
	
	public void replacePaths(){
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder1 = newpath + "\\src\\main\\resources\\env2\\test3.properties";
		String otherFolder2 = newpath + "\\src\\main\\resources\\env";
		copyFile(otherFolder1,otherFolder2);
		
	}
}
