import static util.ScreenUtils.waitForVisibility;

import org.testng.annotations.Test;

import pages.LogIn;
import pages.Profile;
import pages.SplashArt;

public class LogInLogOut extends BaseClass   {
	  @Test ()

	  public void LogInTest() throws InterruptedException {

		  waitForVisibility(SplashArt.SignUpScreen());
		  SplashArt.BtnLogIn().click();
		  LogIn.LoginScreen();
		  LogIn.AreaCode().click();
		  LogIn.SearchAreaCode().click();
		  LogIn.SearchAreaCode().sendKeys(Country);
		  LogIn.ResultAreaCode().click();
		  LogIn.TxtPhoneNumber().click();
		  LogIn.TxtPhoneNumber().sendKeys(Phone);
		  LogIn.BtnNextLogin().click();
		  LogIn.BtnConfirmPhoneNumber().click();
		  LogIn.TxtPassword().click();
		  LogIn.TxtPassword().sendKeys(Password);
		  LogIn.BtnShowPassword().click();
		  LogIn.BtnNext().click();
		  Profile.profileBtn().click();
		  //waitForVisibility(Profile.profileImageView());
		  Profile.logOutBtn().click();
		  Profile.yesLogOutBtn().click();
		  
		  
	  } 
}
