

import static configuration.Config.CONFIG;
import static driver.AppiumServer.APPIUM_SERVICE;
import static driver.DriverManager.DRIVER;

import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class BaseClassiOS 
{
    @BeforeSuite(alwaysRun = true)
    public void beforeTest(ITestContext context) {
        CONFIG.setTestEnvironment();
        DRIVER.setDriver2();
     //   WebDriverRunner.setWebDriver(DRIVER.getDriver2());
        
    }
	
	public void waitForVisibility(WebElement webElement) {
		//  WebDriverWait wait = new WebDriverWait(DRIVER.getDriver2(), ScreenUtils.WAIT);
	//	  wait.until(ExpectedConditions.visibilityOf(webElement));
		  
	  }
	
	public void ClickKeyboard() {
	//	DRIVER.getDriver2().getKeyboard().sendKeys("0");
		  
	  }
	/*@SuppressWarnings("rawtypes")
	public void readOTP() throws InterruptedException
	{
		(() DRIVER.getDriver()).openNotifications();
		Thread.sleep(45000);
		String otp = DRIVER.getDriver().findElementByXPath("//*[contains(@text,'hugoCode')]").getText().split("hugoCode:" )[0];
	    System.out.println(otp);
	}
	*/

    @AfterSuite(alwaysRun = true)
    public void afterTest(final ITestContext testContext) {
        DRIVER.closeDriver();
        APPIUM_SERVICE.stop();
    }
}
