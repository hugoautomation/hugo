import static configuration.Config.CONFIG;
import static driver.AppiumServer.APPIUM_SERVICE;
import static driver.DriverManager.DRIVER;
import static util.ScreenUtils.Setlocation;

import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.codeborne.selenide.WebDriverRunner;



public class BaseClass 

{
    public static String Country = ""; 
	public static String Phone = "";
	public static String Password = "";
	public static String Name = ""; 
	public static String LastName = "";
	public static String Email = "";
	public static String Location = "";
	public static String DescAddress = "";
	public static String DescriptionShipment = "";
	public static String Service = "";
	public static String SearchResult = "";
	public static String Product = "";
	public static String Provider = "";
	public static String NumberContract = "";
	public static String HouseNumber = "";
	public static String CardNumber = "";
	public static String DueDate = "";
	public static String CvcNumber = "";
	public static String PaymentType = "";
	public static String PromoCode = "";
	public static String FreeDeliverys = "";
	public static String Invoice = "";
	public static String NIT = "";
	public static String SearchAddress = "";
	public static String InstructionsOnCheckout = "";
	public static String NeedChange ="";
	public static String Amount ="";
	public static String PaymentLocationMandaditos ="";
	public static String ratingLvl = "";
	public static String rewardMandaditos = "";
	public static String Platform = "";
	public static String OrderType = "";
	public static String SendCode = "";
	public static String pinHugoPay = "";
	public static String Event = "";
	public static String Address = "";
	public static String CategoryHugoMarket= "";
	public static String PhoneOperator= "";
	public static String ServicePagoAServicio= "";
	public static String TypeOfContract= "";
	public static String Category= "";
	public static String Last4NumberCard= "";
	public static String Language = System.getProperty("language");
	public static String ServiceToSwipe1 = "";
	public static String ServiceToSwipe2 = "";
	public static String ServiceToSwipe3 = "";
	public static String ServiceToSwipe4 = "";
	public static String SuscribirPrime ="";
	public static String ServiceOfMastersearch ="";
	
    @BeforeSuite(alwaysRun = true)
    public void beforeTest(ITestContext context) {
        CONFIG.setTestEnvironment();
        Platform = System.getProperty("Platform");
        switch (Platform)
        {
        case "android":
        	DRIVER.setDriver();
        	break;
        case "ios":
        	DRIVER.setDriver2();
        	break;
        case "browserstack":
        	DRIVER.setDriver3();
        	break;
        }
        WebDriverRunner.setWebDriver(DRIVER.getDriver());
        Setlocation();
        Country = System.getProperty("Country");
        Phone =  System.getProperty("Phone");
        Password =  System.getProperty("Password");
    	Name = System.getProperty("Name");
    	LastName = System.getProperty("LastName");
    	Email = System.getProperty("Email");
    	Location = System.getProperty("Location");
    	DescAddress = System.getProperty("DescAddress");
    	DescriptionShipment = System.getProperty("DescriptionShipment");
    	Service = System.getProperty("Service");
    	SearchResult = System.getProperty("SearchResult");
    	Product = System.getProperty("Product");
    	Provider = System.getProperty("Provider");
    	NumberContract = System.getProperty("NumberContract");
    	HouseNumber = System.getProperty("HouseNumber");
    	CardNumber = System.getProperty("CardNumber");
    	DueDate = System.getProperty("DueDate");
    	CvcNumber = System.getProperty("CvcNumber");
    	PaymentType = System.getProperty("PaymentType");
    	PromoCode = System.getProperty("PromoCode");
    	FreeDeliverys = System.getProperty("FreeDeliverys");
    	Invoice = System.getProperty("Invoice");
    	NIT = System.getProperty("NIT");
    	SearchAddress = System.getProperty("SearchAddress"); 
    	InstructionsOnCheckout = System.getProperty("InstructionsOnCheckout");
    	NeedChange = System.getProperty("NeedChange");
    	Amount = System.getProperty("Amount");
    	PaymentLocationMandaditos = System.getProperty("PaymentLocationMandaditos");
    	ratingLvl = System.getProperty("ratingLvl");
    	rewardMandaditos = System.getProperty("rewardMandaditos");
    	OrderType = System.getProperty("OrderType");
    	SendCode = System.getProperty("SendCode");
    	pinHugoPay = System.getProperty("pinHugoPay");
    	Event = System.getProperty("Event");
    	CategoryHugoMarket = System.getProperty("CategoryHugoMarket");
    	ServiceOfMastersearch = System.getProperty("ServiceOfMastersearch");
    	Language = System.getProperty("language");
    	ServiceToSwipe1 = System.getProperty("ServiceToSwipe1");
    	ServiceToSwipe2 = System.getProperty("ServiceToSwipe2");
    	ServiceToSwipe3 = System.getProperty("ServiceToSwipe3");
    	ServiceToSwipe4 = System.getProperty("ServiceToSwipe4");
    	SuscribirPrime= System.getProperty("SuscribirPrime");

    	
    }
	
    @AfterSuite(alwaysRun = true)
    public void afterTest(final ITestContext testContext) {
        DRIVER.closeDriver();
        APPIUM_SERVICE.stop();

    }
}
