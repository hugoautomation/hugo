
import static util.ScreenUtils.ClickKeyboard;
import static util.ScreenUtils.readOTP;
import static util.ScreenUtils.waitForVisibility;

import org.testng.annotations.Test;

import pages.LogIn;
import pages.SignUp;
import pages.SplashArt;

public class SignUpTest extends BaseClass {


	@Test()
	public void RegisterTest() throws InterruptedException {
		// Caso en el que no se tenga cuenta creada
		waitForVisibility(SplashArt.SignUpScreen());
		SplashArt.BtnCreateUser().click();
		SignUp.SignUpScreen().isDisplayed();
		SignUp.SkipScreen().click();
		SignUp.AreaCode().click();
		SignUp.SearchAreaCode().click();
		SignUp.SearchAreaCode().sendKeys(Country);
		SignUp.ResultAreaCode().click();
		SignUp.TxtPhoneNumber().click();
		SignUp.TxtPhoneNumber().sendKeys(Phone);
		if(!SendCode.isEmpty())
		{
			SignUp.btnSendCodeWhatsapp().click();
		}
		else
		{
			SignUp.btnSendCodeSMS().click();
		}
		Thread.sleep(3000);
		
		try {
			// Caso en el que se tenga cuenta creada
			if (SignUp.lblErrorAccountExist().isDisplayed()) {
				SignUp.btnLogIn().click();

				LogIn.AreaCode().click();
				LogIn.SearchAreaCode().click();
				LogIn.SearchAreaCode().sendKeys(Country);
				LogIn.ResultAreaCode().click();
				LogIn.TxtPhoneNumber().click();
				LogIn.TxtPhoneNumber().sendKeys(Phone);
				LogIn.BtnNextLogin().click();
				LogIn.BtnConfirmPhoneNumber().click();
				Thread.sleep(3000);
				// Caso en el que se tenga cuenta creada y tiene contraseņa creada
				if (LogIn.TxtPassword().isDisplayed()) {
					LogIn.TxtPassword().click();
					LogIn.TxtPassword().sendKeys(Phone);
					LogIn.BtnShowPassword().click();
					LogIn.BtnNext().click();
				}
				// Caso en el que tenga cuentra creada pero no contraseņa
				else if (LogIn.smsCodeLayout().isDisplayed()) {
					Thread.sleep(3000);
					String SMSCode = readOTP().toString();
					Thread.sleep(3000);
					ClickKeyboard(SMSCode);

					// Caso en el que tenga cuenta creada y al ingresar el codigo de verificacion
					// nos pida crear contraseņa
					if (LogIn.txtPasswordSMS().isDisplayed()) {
						LogIn.txtPasswordSMS().click();
						ClickKeyboard(Password);
						LogIn.txtconfirmPasswordSMS().click();
						ClickKeyboard(Password);
						LogIn.btnSavePassword().click();
					}
					// Caso en el que se inicie sesion la cuenta y nos muestra la pantalla principal
					else {
						waitForVisibility(LogIn.greetingsScreen());

					}
				}
			}
			// Caso en el que se cree la cuenta normalmente.
			else if (SignUp.TxtPinMessage().isDisplayed()) {

				SignUp.TxtPinMessage().click();
				Thread.sleep(3000);
				String SMSCode = readOTP().toString();
				Thread.sleep(3000);
				ClickKeyboard(SMSCode);
				waitForVisibility(SignUp.txtEdtName());
				SignUp.txtEdtName().sendKeys(Name);
				SignUp.txtLastName().sendKeys(LastName);
				SignUp.txtEmail().sendKeys(Email);
				SignUp.btnDoneStep3().click();
				SignUp.btnSuccesAcount().click();

			} else {
				System.out.println("undefined case");
			}

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

	}
}