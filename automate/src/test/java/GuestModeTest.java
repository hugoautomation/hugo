import static util.ScreenUtils.waitForVisibility;

import org.testng.annotations.Test;

import pages.GuestMode;
import pages.SplashArt;
import util.ScreenUtils;

public class GuestModeTest extends BaseClass {

	@Test()
	public void GuestModeMandaditosTest() throws InterruptedException {
		// Flujo de inicio como Modo invitado
		waitForVisibility(SplashArt.SignUpScreen());
		SplashArt.BtnGuestMode().click();
		ScreenUtils.swipeElementAndroid(GuestMode.onboardingGuestModeScreen(), "LEFT");
		ScreenUtils.swipeElementAndroid(GuestMode.onboardingGuestModeScreen(), "LEFT");
		GuestMode.continueBtnOnboarding().click();
		waitForVisibility(GuestMode.cashbackBtnHome());

		// Busqueda de un servicio
		switch (Service) {
		case "Mandaditos":
			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.specificServiceBtn(), 5);
			waitForVisibility(GuestMode.specificServiceBtn());
			GuestMode.specificServiceBtn().click();
			waitForVisibility(GuestMode.okBtnMandaditos()); 
			GuestMode.okBtnMandaditos().click();
			GuestMode.pickUpAddressTxt().click();
			GuestMode.pickUpTxt().click();
			GuestMode.pickUpTxt().sendKeys(Location);
			waitForVisibility(GuestMode.specificLocationBtn());
			GuestMode.specificLocationBtn().click();
			GuestMode.confirmLocationBtn().click();
			GuestMode.descAddressTxt().click();
			GuestMode.descAddressTxt().sendKeys(DescAddress);
			GuestMode.descriptionShipmentTxt().click();
			GuestMode.descriptionShipmentTxt().sendKeys(DescriptionShipment);
			GuestMode.saveDescriptionBtn().click();
			GuestMode.deliveryAddresTxt().click();
			GuestMode.currentLocationBtn().click();
			GuestMode.confirmLocationBtn().click();
			GuestMode.descAddressTxt().click();
			GuestMode.descAddressTxt().sendKeys(DescAddress);
			GuestMode.descriptionShipmentTxt().click();
			GuestMode.descriptionShipmentTxt().sendKeys(DescriptionShipment);
			GuestMode.saveDescriptionBtn().click();
			GuestMode.shipmentBtn().click();
			GuestMode.createAccountBtn().click();
			break;

		case "Supermercado":

			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.specificServiceBtn(), 5);
			waitForVisibility(GuestMode.specificServiceBtn());
			GuestMode.specificServiceBtn().click();
			waitForVisibility(GuestMode.titleOnService());
			GuestMode.searchTxtOnService().click();
			GuestMode.searchElasticOnService().click();
			GuestMode.searchElasticOnService().sendKeys(SearchResult);
			GuestMode.resultOnSearch().click();
			waitForVisibility(GuestMode.logoScreenMenu());
			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.titleCategoryOnHugoMarket(), 5);
			GuestMode.titleCategoryOnHugoMarket().click();
			waitForVisibility(GuestMode.addProductOnLayout());
			GuestMode.productOnHM().click();
			GuestMode.addToOrderBtn().click();
			GuestMode.createAccountBtn().click();

			break;
			
		case "Recargas":

			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.specificServiceBtn(), 5);
			waitForVisibility(GuestMode.specificServiceBtn());
			GuestMode.specificServiceBtn().click();
			waitForVisibility(GuestMode.recargasAmountBtn());
			GuestMode.editPhoneNumberBtn().click();
			GuestMode.phoneNumberTxt().sendKeys(Phone);
			GuestMode.saveNewPhoneBtn().click();
			GuestMode.operadoraBtn().click();
			ScreenUtils.waitForInvisibility(GuestMode.loadingRecargasLogo());
			GuestMode.continueBtnRecargas().click();
			
			break;
			
		case "Pago de Servicios":

			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.specificServiceBtn(), 5);
			waitForVisibility(GuestMode.specificServiceBtn());
			GuestMode.specificServiceBtn().click();
			waitForVisibility(GuestMode.btnBarCodePagoServicios());
			GuestMode.serviceOnPagoServicios().click();
			GuestMode.searchProviderTxt().sendKeys(Provider);
			GuestMode.clickOnResultProvider().click();
			
			Thread.sleep(5000);
			
			if(GuestMode.listOfContracts().isDisplayed())
			{
				GuestMode.clickOnListOfContracts().click();
				GuestMode.clickTypeOfContract().click();
				GuestMode.numberContractTxt().sendKeys(NumberContract);
				GuestMode.validateContractNumber().click();
				GuestMode.createAccountBtn().click();
			}
			else if(GuestMode.readCodeBtn().isDisplayed())
			{
				GuestMode.readCodeBtn().click();
				GuestMode.createAccountBtn().click();
			}
			else if(GuestMode.numberContractTxt().isDisplayed() && !GuestMode.listOfContracts().isDisplayed())
			{
				GuestMode.numberContractTxt().sendKeys(NumberContract);
				GuestMode.validateContractNumber().click();
				GuestMode.createAccountBtn().click();
			}
			
			break;

		default:
			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.specificServiceBtn(), 5);
			waitForVisibility(GuestMode.specificServiceBtn());
			GuestMode.specificServiceBtn().click();
			waitForVisibility(GuestMode.titleOnService());
			GuestMode.searchTxtOnService().click();
			GuestMode.searchElasticOnService().click();
			GuestMode.searchElasticOnService().sendKeys(SearchResult);
			GuestMode.resultOnSearch().click();
			waitForVisibility(GuestMode.logoScreenMenu());
			ScreenUtils.findItemWithScrollingUsingBy(GuestMode.productSearch(), 5);
			GuestMode.productSearch().click();
			GuestMode.addToOrderBtn().click();
			GuestMode.createAccountBtn().click();
			break;

		}

		
	}

}
