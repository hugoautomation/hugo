import org.testng.annotations.Test;

import pages.HomePage;
import pages.LogIn;
import pages.SignUp;
import pages.SplashArt;

import static util.ScreenUtils.*;



public class SignInTest extends BaseClass {


	@Test()
	public void LogInTest() throws InterruptedException {
		//Setlocation();
		// Caso Normal de inicio de sesion
		Setlocation();
		Thread.sleep(3000);
		ClevertapDialog();
		waitForVisibility(HomePage.profileBtnHome());
		HomePage.profileBtnHome().click();
		
		
		if (SplashArt.BtnCreateUser().isDisplayed())
		{
			SignWorkFlow();
		}
		else {
			//En Caso de que se agregue otro flujo en futuro
		}
	}
	
	public void SignWorkFlow() throws InterruptedException{

		SplashArt.BtnLogIn().click();
		LogIn.LoginScreen();
		for (int i =0; i <=2; i++)
		{
			Thread.sleep(2000);
		if(!(LogIn.AreaCode().getText() == "+503"))
		{
			LogIn.AreaCode().click();
			LogIn.SearchAreaCode().click();
			LogIn.SearchAreaCode().sendKeys(Country);
			waitForVisibility(LogIn.ResultAreaCode());
			LogIn.ResultAreaCode().click();
			
		}
		}
		LogIn.TxtPhoneNumber().click();
		LogIn.TxtPhoneNumber().sendKeys(Phone);
		LogIn.BtnNextLogin().click();
		LogIn.BtnConfirmPhoneNumber().click();
		Setlocation();
		Thread.sleep(3000);
		
		try {
			// Caso en el que la cuente tenga contraseņa creada
			if (LogIn.TxtPassword().isDisplayed()) {
				LogIn.TxtPassword().click();
				LogIn.TxtPassword().sendKeys(Password);
				LogIn.BtnShowPassword().click();
				LogIn.BtnNext().click();
			}
			// Caso en el que la cuenta no tenga contraseņa creada
			else if (LogIn.smsCodeLayout().isDisplayed()) {
				Thread.sleep(3000);
				String SMSCode = readOTP().toString();
				Thread.sleep(3000);
				ClickKeyboard(SMSCode);
				Thread.sleep(2500);
				// Caso en el que la cuenta no tenga contraseņa creada y al ingresar el codigo
				// de verificacion luego le pida contraseņa
			if (LogIn.txtPasswordSMS().isDisplayed()) {
					LogIn.txtPasswordSMS().click();
					ClickKeyboard(Password);
					LogIn.txtconfirmPasswordSMS().click();
					ClickKeyboard(Password);
					LogIn.btnSavePassword().click();
				}
				// Caso en el que al ingresar lo mande directo a la pantalla principal
				else {
					waitForVisibility(LogIn.greetingsScreen());

				}
			}
			// Caso en el que no se tenga cuenta creada
			else if (LogIn.createAccounBtn().isDisplayed()) {

				LogIn.createAccounBtn().click();
				waitForVisibility(SignUp.AreaCode());
				SignUp.AreaCode().click();
				SignUp.SearchAreaCode().click();
				SignUp.SearchAreaCode().sendKeys(Country);
				SignUp.ResultAreaCode().click();
				SignUp.TxtPhoneNumber().click();
				SignUp.TxtPhoneNumber().sendKeys(Phone);
				SignUp.BtnNextStep1().click();
				SignUp.TxtPinMessage().click();
				Thread.sleep(3000);
				String SMSCode = readOTP().toString();
				Thread.sleep(3000);
				ClickKeyboard(SMSCode);
				waitForVisibility(SignUp.txtEdtName());
				SignUp.txtEdtName().sendKeys(Name);
				SignUp.txtLastName().sendKeys(LastName);
				SignUp.txtEmail().sendKeys(Email);
				SignUp.btnDoneStep3().click();
				SignUp.btnSuccesAcount().click();
			} else {
				System.out.println("undefined case");
			}
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}

	}
	public void ClevertapDialog() throws InterruptedException {
		if(HomePage.clevertapDialog().isDisplayed())
		{
			backAndroidButton();
		}
		if(HomePage.clevertapDialog2().isDisplayed())
		{
			backAndroidButton();
		}
	}
	}



