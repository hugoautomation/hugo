import java.io.File;

import static configuration.Config.CONFIG;
import static util.ScreenUtils.*;

import org.testng.annotations.Test;

import pages.HomePage;
import util.ScreenUtils;

public class MultipleServiceTest extends BaseClass {
//invocationCount = 3
	
	
	
	
	@Test()
	public void HomePageTest() throws InterruptedException {
		// Flujo de inicio de Sesion
		// SignInTest Login = new SignInTest();
		// Login.LogInTest();

		Setlocation();

		for(int i = 1; i<=3; i++)
		{
		PopulateVariables();
		waitForVisibility(HomePage.hugoDeliveryText());
		Thread.sleep(3000);
		// Evaluacion dependiente del servicio, es el flujo que se realizara
		switch (Service) {
//------------------------------------HugoCash--------------------------------------//		
		case "HUGOCASH":
			// Todo servicio que cumpla las mismas condiciones de HugoCash
			HugoCashWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//-----------------------------HugoPay----------------------------------------------//
		case "HUGOPAY":
			HugoPayWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//---------------------------Mandaditos--------------------------------------------//		
		case "MANDADITOS":
			// Flujo de Mandaditos
			MandaditosWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//----------------------------Recargas--------------------------------------------//
		case "RECARGAS":
			// Flujo de Recargas
			RecargasWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//------------------------Pago de Servicios---------------------------------------//
		case "PAGO DE SERVICIOS":
			// Flujo de Pago a Servicios
			PagosdeServiciosWorkFlow();
			CleanVariables();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//------------------------Supermercado--------------------------------------------//
		case "SUPERMERCADO":
			// Todo servicio que cumpla las mismas condiciones de Supermercado.
			SupermercadoWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//------------------------HugoFun--------------------------------------------//	
		case "HUGOFUN":
			//Flujo de Hugo Fun
			HugoFunWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
//-----------------------Cualquier otro servicio Delivery------------------------//
		default:
			// Todo servicio que cumpla las mismas condiciones de Comida.

			DeliveryWorkFlow();
			deleteFile();
			replacePaths(i);
			renameFile(i);
			CleanVariables();
			CONFIG.setTestEnvironment2();
			break;
		}
		
		}
	}
	
	
	
	public void HugoFunWorkFlow() throws InterruptedException{
		
		ScreenUtils.SearchService();
		HomePage.specificServiceBtn().click();
		HomePage.hugoFunBtn().click();
		waitForVisibility(HomePage.logoHugoFun());
		HomePage.searchTxtHugoFun().click();
		HomePage.searchOnTextHugoFun().click();
		HomePage.searchOnTextHugoFun().sendKeys(Event);
		HomePage.selectResultHugoFun().click();
		HomePage.availabilityOnEventHugoFun().click();
		for(int i=0; i <=20; i++)
		{
			if(HomePage.moreTktHugoFun().isDisplayed())
			{
				HomePage.moreTktHugoFun().click();
				break;
			}
			else {
				swipeElementAndroid(HomePage.dateBoxHugoFun(),"LEFT");
				HomePage.dateBoxHugoFun().click();	
			}
			
		}
		HomePage.nextHugoFun().click();
		for(int i=0; i<=20;i++)
		{
			if(HomePage.selectCardHugoFun().isDisplayed())
			{
				HomePage.selectCardHugoFun().click();
				break;
				}
			else
			{
				swipeElementAndroid(HomePage.swipeCardHugoFun(),"LEFT");
			}
		}
		Thread.sleep(3000);
		findItemOnList(HomePage.payFunHugoFun(),15);
		HomePage.payFunHugoFun().click();
		
	}
	
	public void SupermercadoWorkFlow() throws InterruptedException {
		// Flujo de Supermercado
		ScreenUtils.SearchService();
		HomePage.specificServiceBtn().click();
		// Validacion si aparece un raiting de una orden pasada
		for (int i = 0; i <= 20; i++) {
			if (HomePage.ratingPartner().isDisplayed()) {
				HomePage.ratingPartner().click();
				HomePage.ratingDriver().click();
				HomePage.ratingHugo().click();
				// Validacion de si aparece la propina en un flujo normal de Supermercado
				if (HomePage.tipDriver().isDisplayed()) {
					waitForVisibility(HomePage.tipDriver());
					HomePage.tipDriver().click();
					HomePage.tip1Driver().click();
					HomePage.thanksBtnTip().click();
				}
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.skipRaitingBtn(), 20);
				HomePage.skipRaitingBtn().click();
			}
		}
		// Flujo normal de Supermercado sin raiting y sin pagar propina
		waitForVisibility(HomePage.titleOnService());
		//HomePage.searchTxtOnService().click();

		HomePage.newuisearchTxtOnService().click();
		//Implementacion del Mastersearch On Service
		HomePage.searchMasterSearch().click();
		HomePage.searchMasterSearch().sendKeys(SearchResult);
		waitForVisibility(HomePage.resultMasterSearch());
		HomePage.resultMasterSearch().click();
		
		waitForVisibility(HomePage.logoScreenMenu());
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.titleCategoryOnHugoMarket(), 10);
		if(!HomePage.viewOrderBtn().isDisplayed())
		{
			HomePage.titleCategoryOnHugoMarket().click();
			waitForVisibility(HomePage.addProductOnLayout());
		}
		
		// Validacion de si al buscar un producto, se encuentra un Draft ya realizado,
				// pues lo finalizara
				if (HomePage.viewOrderBtn().isDisplayed()) {
					HomePage.viewOrderBtn().click();
					waitForVisibility(HomePage.productImage());
					HomePage.processOrderBtn().click();
				}
				
				
				else {
					HomePage.productOnHM().click();
					HomePage.addToOrderBtn().click();
					Thread.sleep(2000);
					// Validacion de si al agregar productos al draft ya existe uno, que se cancele
					// el anterior
					if (HomePage.alertDialog().isDisplayed()) {
						HomePage.alertDialog().click();
						waitForInvisibility(HomePage.loadingRecargasLogo());
					}
					// Validacion de que si da error al agregar producto se realice un back y poder
					// ver la orden
					if (!HomePage.viewOrderBtn().isDisplayed()) {
						ScreenUtils.backAndroidButton();
					}
					
					HomePage.viewOrderBtn().click();
					waitForVisibility(HomePage.productImage());
					HomePage.processOrderBtn().click();
		
				}
				
		for (int i = 0; i <= 100; i++) {
			if (HomePage.acceptSimilarProductsBtn().isDisplayed()) {
				HomePage.acceptSimilarProductsBtn().click();
				if (HomePage.alertMinimumPurchase().isDisplayed()) {
					HomePage.acceptAlertMinimumPurchase().click();
					HomePage.closeDialogSimilarProduct().click();
					HomePage.addMoreOfTheSameProduct().click();
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.moreProductsBtn(), 5);
					HomePage.moreProductsBtn().click();
					HomePage.moreProductsBtn().click();
					HomePage.addToOrderBtn().click();
					waitForVisibility(HomePage.productImage());
					HomePage.processOrderBtn().click();
				}
			}
			if (HomePage.alertMinimumPurchase().isDisplayed()) {
				HomePage.acceptAlertMinimumPurchase().click();
				HomePage.addMoreOfTheSameProduct().click();
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.moreProductsBtn(), 5);
				HomePage.moreProductsBtn().click();
				HomePage.moreProductsBtn().click();
				HomePage.addToOrderBtn().click();
				waitForVisibility(HomePage.productImage());
				HomePage.processOrderBtn().click();
			}
			if (!HomePage.acceptSimilarProductsBtn().isDisplayed()
					&& !HomePage.acceptSimilarProductsBtn().isDisplayed()) {
				break;
			}

		}
		// Validacion por el tipo de pago
		switch (PaymentType) {

		case "Cash":
			// Flujo de pago en efectivo
			if (!HomePage.findCashTxt().isDisplayed()) {
				CardListWithoutCash();
			} else {
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.findCashTxt(), 20);
				HomePage.findCashTxt().click();
			}
			HomePage.continueBtnPaymentMethod().click();
			waitForVisibility(HomePage.logoCheckoutImage());
			// Validacion si se usara codigo promocional en el checkout
			if (!PromoCode.isEmpty()) {
				waitForVisibility(HomePage.promoCodeBtn());
				HomePage.promoCodeBtn().click();
				HomePage.promoCodeTxt().click();
				ClickKeyboard(PromoCode);
				HomePage.promoReadyBtn().click();
				HomePage.successfullPromoBtn().click();
			}
			// Validacion para ingresar un free delivery

			if (!FreeDeliverys.isEmpty()) {
				Thread.sleep(3000);
				if (HomePage.prizesBtn().isDisplayed()) {
					HomePage.prizesBtn().click();
					HomePage.freeDeliveryBtn().click();
					HomePage.selectAwardBtn().click();
					if (HomePage.successfullPromoBtn().isDisplayed()) {
						HomePage.successfullPromoBtn().click();
					}
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.invoiceBtn(), 20);
			// Validacion para agregar una factura
			if (!Invoice.isEmpty()) {
				waitForVisibility(HomePage.invoiceBtn());
				HomePage.invoiceBtn().click();
				// Validacion para verificar si la factura no existe, que la cree
				if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
					HomePage.addInvoiceBtn().click();
					waitForVisibility(HomePage.nameInvoiceTxt());
					HomePage.nameInvoiceTxt().sendKeys(Name);
					HomePage.nitInvoiceTxt().sendKeys(NIT);
					HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
					HomePage.emailInvoiceTxt().sendKeys(Email);
					HomePage.saveInvoiceBtn().click();
					waitForVisibility(HomePage.logoCheckoutImage());
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				} else {
					// Si existiera la factura solo la seleccione
					for (int i = 0; i <= 5; i++) {
						HomePage.selectInvoiceBtn().click();
						if (HomePage.useInvoiceBtn().isDisplayed()) {
							HomePage.useInvoiceBtn().click();
							break;
						}
					}
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);

			// Validacion si la orden es programada
			if (!OrderType.isEmpty()) {
				waitForVisibility(HomePage.scheduleOrderBtn());
				HomePage.scheduleOrderBtn().click();
				HomePage.btnAgreeScheduleOrder().click();
				// Flujo de pago en Tarjeta de credito por Orden Programada
				// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
				Thread.sleep(3000);
				if (HomePage.addCardInScheduleOrder().isDisplayed()) {
					for (int i = 0; i <= 2; i++) {
						if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
							HomePage.addCardInScheduleOrder().click();
							HomePage.cardHolderTxt().click();
							HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
							ScreenUtils.backAndroidButton();
							HomePage.cardNumberTxt().click();
							ClickKeyboard(CardNumber);
							ScreenUtils.backAndroidButton();
							HomePage.dueDateTxt().click();
							ClickKeyboard(DueDate);
							ScreenUtils.backAndroidButton();
							HomePage.identifierTxt().sendKeys(Name + " " + LastName);
							ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
							HomePage.whiteColorRadioBtn().click();
							HomePage.saveCardBtn().click();
							Thread.sleep(2000);
							// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
							if (HomePage.cvcPinTxt().isDisplayed()) {
								HomePage.cvcPinTxt().sendKeys(CvcNumber);
							}
							HomePage.confirmSaveCardBtn().click();
						}

					}
					HomePage.findCardTxt().click();
					HomePage.continueBtnPaymentMethod().click();
				}
				waitForVisibility(HomePage.policiesHugoPay());
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);
			}

			// Validacion si se necesita agregar instrucciones
			if (!InstructionsOnCheckout.isEmpty()) {
				waitForVisibility(HomePage.instructionsOnCheckoutTxt());
				HomePage.instructionsOnCheckoutTxt().click();
				HomePage.instructionsOnCheckoutTxt().sendKeys(InstructionsOnCheckout);
				ScreenUtils.backAndroidButton();
			}

			HomePage.processOrderStep1().click();
			// Validacion si se nos solicita cambio para nuestrea orden
			if (HomePage.changeTxt().isDisplayed()) {

				if (!NeedChange.isEmpty()) {
					HomePage.changeTxt().click();
					HomePage.changeTxt().sendKeys(NeedChange);
					HomePage.readyChangeBtn().click();
				} else {
					HomePage.dontNeedChangeBtn().click();
				}

			}
			// Validacion si se nos solicita el ingreso de CVC al pagar por ser orden
			// programada
			if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

				ClickKeyboard(CvcNumber);
			}

			HomePage.confirmLocation().click();
			waitForVisibility(HomePage.goToTrackingBtn());
			HomePage.goToTrackingBtn().click();
			if (OrderType.isEmpty()) {
				ScreenUtils.backAndroidButton();
				ScreenUtils.backAndroidButton();
			}
			break;

		case "Card":
			// Flujo de pago en Tarjeta de credito
			// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
			for (int i = 0; i <= 2; i++) {
				if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
					HomePage.addCardBtnOnProcessOrder().click();
					HomePage.cardHolderTxt().click();
					HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.backAndroidButton();
					HomePage.cardNumberTxt().click();
					ClickKeyboard(CardNumber);
					ScreenUtils.backAndroidButton();
					HomePage.dueDateTxt().click();
					ClickKeyboard(DueDate);
					ScreenUtils.backAndroidButton();
					HomePage.identifierTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
					HomePage.whiteColorRadioBtn().click();
					HomePage.saveCardBtn().click();
					Thread.sleep(2000);
					// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
					if (HomePage.cvcPinTxt().isDisplayed()) {
						HomePage.cvcPinTxt().sendKeys(CvcNumber);
					}
					HomePage.confirmSaveCardBtn().click();
				}

			}
			// Flujo al tener la tarjeta de credito especifica creada
			HomePage.findCardTxt().click();
			Thread.sleep(3000);
			HomePage.continueBtnPaymentMethod().click();
			waitForVisibility(HomePage.logoCheckoutImage());
			// Validacion si se usara codigo promocional en el checkout
			if (!PromoCode.isEmpty()) {
				waitForVisibility(HomePage.promoCodeBtn());
				HomePage.promoCodeBtn().click();
				HomePage.promoCodeTxt().click();
				ClickKeyboard(PromoCode);
				HomePage.promoReadyBtn().click();
				HomePage.successfullPromoBtn().click();
			}
			// Validacion para ingresar un free delivery

			if (!FreeDeliverys.isEmpty()) {
				Thread.sleep(5000);
				if (HomePage.prizesBtn().isDisplayed()) {
					HomePage.prizesBtn().click();
					HomePage.freeDeliveryBtn().click();
					HomePage.selectAwardBtn().click();
					if (HomePage.successfullPromoBtn().isDisplayed()) {
						HomePage.successfullPromoBtn().click();
					}
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.invoiceBtn(), 20);

			// Validacion para agregar una factura
			if (!Invoice.isEmpty()) {
				waitForVisibility(HomePage.invoiceBtn());
				HomePage.invoiceBtn().click();
				// Validacion para verificar si la factura no existe, que la cree
				if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
					HomePage.addInvoiceBtn().click();
					waitForVisibility(HomePage.nameInvoiceTxt());
					HomePage.nameInvoiceTxt().sendKeys(Name);
					HomePage.nitInvoiceTxt().sendKeys(NIT);
					HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
					HomePage.emailInvoiceTxt().sendKeys(Email);
					HomePage.saveInvoiceBtn().click();
					waitForVisibility(HomePage.logoCheckoutImage());
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				} else {
					// Si existiera la factura solo la seleccione
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);

			// Validacion si la orden es programada
			if (!OrderType.isEmpty()) {
				waitForVisibility(HomePage.scheduleOrderBtn());
				HomePage.scheduleOrderBtn().click();
				HomePage.btnAgreeScheduleOrder().click();

			}

			// Validacion si se necesita agregar instrucciones
			if (!InstructionsOnCheckout.isEmpty()) {
				waitForVisibility(HomePage.instructionsOnCheckoutTxt());
				HomePage.instructionsOnCheckoutTxt().click();
				HomePage.instructionsOnCheckoutTxt().sendKeys(InstructionsOnCheckout);
				ScreenUtils.backAndroidButton();
			}

			HomePage.processOrderStep1().click();
			// Validacion si se nos solicita el ingreso de CVC al pagar
			if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

				ClickKeyboard(CvcNumber);
			}
			HomePage.confirmLocation().click();
			waitForVisibility(HomePage.goToTrackingBtn());
			HomePage.goToTrackingBtn().click();
			if (OrderType.isEmpty()) {
				ScreenUtils.backAndroidButton();
				ScreenUtils.backAndroidButton();
			}

			break;
		}
	}

	
	public void DeliveryWorkFlow() throws InterruptedException {
		// Todo servicio que cumpla las mismas condiciones de Comida.
		ScreenUtils.SearchService();
		HomePage.specificServiceBtn().click();
		Thread.sleep(2500);
		// Validacion si aparece un raiting de una orden pasada
		for (int i = 0; i <= 20; i++) {
			if (HomePage.ratingPartner().isDisplayed()) {
				HomePage.ratingPartner().click();
				HomePage.ratingDriver().click();
				HomePage.ratingHugo().click();
				// Validacion de si aparece la propina en un flujo normal de Delivery
				if (HomePage.tipDriver().isDisplayed()) {
					waitForVisibility(HomePage.tipDriver());
					HomePage.tipDriver().click();
					HomePage.tip1Driver().click();
					HomePage.thanksBtnTip().click();
				}
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.skipRaitingBtn(), 20);
				HomePage.skipRaitingBtn().click();
			}
		}
		// Flujo normal de Delivery sin raiting y sin pagar propina
		waitForVisibility(HomePage.titleOnService());

		HomePage.newuisearchTxtOnService().click();
		//Implementacion del Mastersearch On Service
		HomePage.searchMasterSearch().click();
		HomePage.searchMasterSearch().sendKeys(SearchResult);
		waitForVisibility(HomePage.resultMasterSearch());
		HomePage.resultMasterSearch().click();
		
		waitForVisibility(HomePage.logoScreenMenu());
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.productSearch(), 100);
		// Validacion de si al buscar un producto, se encuentra un Draft ya realizado,
		// pues lo finalizara
		if (HomePage.viewOrderBtn().isDisplayed()) {
			HomePage.viewOrderBtn().click();
			waitForVisibility(HomePage.productImage());
			HomePage.processOrderBtn().click();
		}
		// Validacion de si no encuentra ni un Draft, continuara con la Orden
		else {
			HomePage.productSearch().click();
			Thread.sleep(3000);
			HomePage.addToOrderBtn().click();
			Thread.sleep(2000);
			// Validacion de que al agregar el producto, nos muestre una alerta indicandonos
			// que ya tenemos un draft y lo queremos eliminar para hacer uno nuevo
			if (HomePage.alertDialog().isDisplayed()) {
				HomePage.alertDialog().click();
				waitForInvisibility(HomePage.loadingRecargasLogo());
			}
			// Validacion para cuando se encuentre en la pantalla del detalle del producto
			// especifico, al seleccionar la opcion de agregar producto nos muestra error,
			// se dara back para visualizar el "Ver Orden"
			if (!HomePage.viewOrderBtn().isDisplayed()) {
				ScreenUtils.backAndroidButton();
			}
			HomePage.viewOrderBtn().click();
			waitForVisibility(HomePage.productImage());
			HomePage.processOrderBtn().click();
		}

		// Validacion por el tipo de pago
		switch (PaymentType) {

		case "Cash":
			// Flujo de pago en efectivo
			if (!HomePage.findCashTxt().isDisplayed()) {
				CardListWithoutCash();
			} else {
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.findCashTxt(), 20);
				HomePage.findCashTxt().click();
			}
			HomePage.continueBtnPaymentMethod().click();
			waitForVisibility(HomePage.logoCheckoutImage());
			// Validacion si se usara codigo promocional en el checkout
			if (!PromoCode.isEmpty()) {
				waitForVisibility(HomePage.promoCodeBtn());
				HomePage.promoCodeBtn().click();
				HomePage.promoCodeTxt().click();
				ClickKeyboard(PromoCode);
				HomePage.promoReadyBtn().click();
				HomePage.successfullPromoBtn().click();
			}
			// Validacion para ingresar un free delivery

			if (!FreeDeliverys.isEmpty()) {
				Thread.sleep(3000);
				if (HomePage.prizesBtn().isDisplayed()) {
					HomePage.prizesBtn().click();
					HomePage.freeDeliveryBtn().click();
					HomePage.selectAwardBtn().click();
					if (HomePage.successfullPromoBtn().isDisplayed()) {
						HomePage.successfullPromoBtn().click();
					}
				}
			}

			ScreenUtils.findItemWithScrollingUsingBy(HomePage.invoiceBtn(), 20);
			// Validacion para agregar una factura
			if (!Invoice.isEmpty()) {
				waitForVisibility(HomePage.invoiceBtn());
				HomePage.invoiceBtn().click();
				// Validacion para verificar si la factura no existe, que la cree
				if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
					HomePage.addInvoiceBtn().click();
					waitForVisibility(HomePage.nameInvoiceTxt());
					HomePage.nameInvoiceTxt().sendKeys(Name);
					HomePage.nitInvoiceTxt().sendKeys(NIT);
					HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
					HomePage.emailInvoiceTxt().sendKeys(Email);
					HomePage.saveInvoiceBtn().click();
					waitForVisibility(HomePage.logoCheckoutImage());
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				} else {
					// Si existiera la factura solo la seleccione
					for (int i = 0; i <= 5; i++) {
						HomePage.selectInvoiceBtn().click();
						if (HomePage.useInvoiceBtn().isDisplayed()) {
							HomePage.useInvoiceBtn().click();
							break;
						}
					}
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);

			// Validacion si la orden es programada
			if (!OrderType.isEmpty()) {
				waitForVisibility(HomePage.scheduleOrderBtn());
				HomePage.scheduleOrderBtn().click();
				HomePage.btnAgreeScheduleOrder().click();
				// Flujo de pago en Tarjeta de credito por Orden Programada
				// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
				Thread.sleep(3000);
				if (HomePage.addCardInScheduleOrder().isDisplayed()) {
					for (int i = 0; i <= 2; i++) {
						if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
							HomePage.addCardInScheduleOrder().click();
							HomePage.cardHolderTxt().click();
							HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
							ScreenUtils.backAndroidButton();
							HomePage.cardNumberTxt().click();
							ClickKeyboard(CardNumber);
							ScreenUtils.backAndroidButton();
							HomePage.dueDateTxt().click();
							ClickKeyboard(DueDate);
							ScreenUtils.backAndroidButton();
							HomePage.identifierTxt().sendKeys(Name + " " + LastName);
							ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
							HomePage.whiteColorRadioBtn().click();
							HomePage.saveCardBtn().click();
							Thread.sleep(2000);
							// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
							if (HomePage.cvcPinTxt().isDisplayed()) {
								HomePage.cvcPinTxt().sendKeys(CvcNumber);
							}
							HomePage.confirmSaveCardBtn().click();
						}

					}
					HomePage.findCardTxt().click();
					HomePage.continueBtnPaymentMethod().click();
				}
				waitForVisibility(HomePage.policiesHugoPay());
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);
			}

			// Validacion si se necesita agregar instrucciones
			if (!InstructionsOnCheckout.isEmpty()) {
				waitForVisibility(HomePage.instructionsOnCheckoutTxt());
				HomePage.instructionsOnCheckoutTxt().click();
				HomePage.instructionsOnCheckoutTxt().sendKeys(InstructionsOnCheckout);
				ScreenUtils.backAndroidButton();
			}

			HomePage.processOrderStep1().click();
			// Validacion si se nos solicita cambio para nuestrea orden
			if (HomePage.changeTxt().isDisplayed()) {

				if (!NeedChange.isEmpty()) {
					HomePage.changeTxt().click();
					HomePage.changeTxt().sendKeys(NeedChange);
					HomePage.readyChangeBtn().click();
				} else {
					HomePage.dontNeedChangeBtn().click();
				}

			}
			// Validacion si se nos solicita el ingreso de CVC al pagar por ser orden
			// programada
			if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

				ClickKeyboard(CvcNumber);
			}

			HomePage.confirmLocation().click();
			waitForVisibility(HomePage.goToTrackingBtn());
			HomePage.goToTrackingBtn().click();
			if (OrderType.isEmpty()) {
				ScreenUtils.backAndroidButton();
				ScreenUtils.backAndroidButton();
			}
			break;

		case "Card":
			// Flujo de pago en Tarjeta de credito
			// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
			for (int i = 0; i <= 2; i++) {
				if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
					HomePage.addCardBtnOnProcessOrder().click();
					HomePage.cardHolderTxt().click();
					HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.backAndroidButton();
					HomePage.cardNumberTxt().click();
					ClickKeyboard(CardNumber);
					ScreenUtils.backAndroidButton();
					HomePage.dueDateTxt().click();
					ClickKeyboard(DueDate);
					ScreenUtils.backAndroidButton();
					HomePage.identifierTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
					HomePage.whiteColorRadioBtn().click();
					HomePage.saveCardBtn().click();
					Thread.sleep(2000);
					// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
					if (HomePage.cvcPinTxt().isDisplayed()) {
						HomePage.cvcPinTxt().sendKeys(CvcNumber);
					}
					HomePage.confirmSaveCardBtn().click();
				}

			}
			// Flujo al tener la tarjeta de credito especifica creada
			HomePage.findCardTxt().click();
			Thread.sleep(3000);
			HomePage.continueBtnPaymentMethod().click();
			waitForVisibility(HomePage.logoCheckoutImage());
			// Validacion si se usara codigo promocional en el checkout
			if (!PromoCode.isEmpty()) {
				waitForVisibility(HomePage.promoCodeBtn());
				HomePage.promoCodeBtn().click();
				HomePage.promoCodeTxt().click();
				ClickKeyboard(PromoCode);
				HomePage.promoReadyBtn().click();
				HomePage.successfullPromoBtn().click();
			}
			// Validacion para ingresar un free delivery

			if (!FreeDeliverys.isEmpty()) {
				Thread.sleep(5000);
				if (HomePage.prizesBtn().isDisplayed()) {
					HomePage.prizesBtn().click();
					HomePage.freeDeliveryBtn().click();
					HomePage.selectAwardBtn().click();
					if (HomePage.successfullPromoBtn().isDisplayed()) {
						HomePage.successfullPromoBtn().click();
					}
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.invoiceBtn(), 20);

			// Validacion para agregar una factura
			if (!Invoice.isEmpty()) {
				waitForVisibility(HomePage.invoiceBtn());
				HomePage.invoiceBtn().click();
				// Validacion para verificar si la factura no existe, que la cree
				if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
					HomePage.addInvoiceBtn().click();
					waitForVisibility(HomePage.nameInvoiceTxt());
					HomePage.nameInvoiceTxt().sendKeys(Name);
					HomePage.nitInvoiceTxt().sendKeys(NIT);
					HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
					HomePage.emailInvoiceTxt().sendKeys(Email);
					HomePage.saveInvoiceBtn().click();
					waitForVisibility(HomePage.logoCheckoutImage());
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				} else {
					// Si existiera la factura solo la seleccione
					HomePage.selectInvoiceBtn().click();
					HomePage.useInvoiceBtn().click();
				}
			}
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);

			// Validacion si la orden es programada
			if (!OrderType.isEmpty()) {
				waitForVisibility(HomePage.scheduleOrderBtn());
				HomePage.scheduleOrderBtn().click();
				HomePage.btnAgreeScheduleOrder().click();

			}

			// Validacion si se necesita agregar instrucciones
			if (!InstructionsOnCheckout.isEmpty()) {
				waitForVisibility(HomePage.instructionsOnCheckoutTxt());
				HomePage.instructionsOnCheckoutTxt().click();
				HomePage.instructionsOnCheckoutTxt().sendKeys(InstructionsOnCheckout);
				ScreenUtils.backAndroidButton();
			}

			HomePage.processOrderStep1().click();
			// Validacion si se nos solicita el ingreso de CVC al pagar
			if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

				ClickKeyboard(CvcNumber);
			}
			HomePage.confirmLocation().click();
			waitForVisibility(HomePage.goToTrackingBtn());
			HomePage.goToTrackingBtn().click();
			if (OrderType.isEmpty()) {
				ScreenUtils.backAndroidButton();
				ScreenUtils.backAndroidButton();
			}

			break;
		}

	}

	public void CardListWithoutCash() throws InterruptedException {

		// Flujo de pago en Tarjeta de credito
		// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
		for (int i = 0; i <= 2; i++) {
			if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
				HomePage.addCardBtnOnProcessOrder().click();
				HomePage.cardHolderTxt().click();
				HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.backAndroidButton();
				HomePage.cardNumberTxt().click();
				ClickKeyboard(CardNumber);
				ScreenUtils.backAndroidButton();
				HomePage.dueDateTxt().click();
				ClickKeyboard(DueDate);
				ScreenUtils.backAndroidButton();
				HomePage.identifierTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
				HomePage.whiteColorRadioBtn().click();
				HomePage.saveCardBtn().click();
				Thread.sleep(2000);
				// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
				if (HomePage.cvcPinTxt().isDisplayed()) {
					HomePage.cvcPinTxt().sendKeys(CvcNumber);
				}
				HomePage.confirmSaveCardBtn().click();
			}

		}
		// Flujo al tener la tarjeta de credito especifica creada
		HomePage.findCardTxt().click();
	}

	public void PagosdeServiciosWorkFlow() throws InterruptedException {
		ScreenUtils.SearchService();
		waitForVisibility(HomePage.specificServiceBtn());
		HomePage.specificServiceBtn().click();
		waitForVisibility(HomePage.btnBarCodePagoServicios());
		HomePage.serviceOnPagoServicios().click();
		HomePage.searchProviderTxt().sendKeys(Provider);
		HomePage.clickOnResultProvider().click();
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());

		if (HomePage.listOfContracts().isDisplayed()) {
			HomePage.clickOnListOfContracts().click();
			HomePage.clickTypeOfContract().click();
			HomePage.numberContractTxt().sendKeys(NumberContract);
			HomePage.validateContractNumber().click();
		} else if (HomePage.readCodeBtn().isDisplayed()) {
			HomePage.readCodeBtn().click();
		} else if (HomePage.numberContractTxt().isDisplayed() && !HomePage.listOfContracts().isDisplayed()) {
			HomePage.numberContractTxt().sendKeys(NumberContract);
			HomePage.validateContractNumber().click();
		}
	}

	public void RecargasWorkFlow() throws InterruptedException {
		ScreenUtils.SearchService();
		waitForVisibility(HomePage.specificServiceBtn());
		HomePage.specificServiceBtn().click();
		waitForVisibility(HomePage.recargasAmountBtn());
		HomePage.editPhoneNumberBtn().click();
		HomePage.phoneNumberTxt().sendKeys(Phone);
		HomePage.saveNewPhoneBtn().click();
		HomePage.operadoraBtn().click();
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
		waitForVisibility(HomePage.recargasAmountBtn());
		switch (Amount) {
		case "1":
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "RIGHT");
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "RIGHT");
			break;
		case "2":
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "RIGHT");
			break;
		case "5":
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "RIGHT");
			break;
		case "10":
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "LEFT");
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "LEFT");
			break;
		case "20":
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "LEFT");
			ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "LEFT");
			//ScreenUtils.swipeElementByPixels(HomePage.amountCarrusel(), "LEFT");
			break;
		default:
			break;

		}
		HomePage.continueBtnRecargas().click();
		waitForVisibility(HomePage.providerLogoIMG());
		Thread.sleep(2000);
	/*	HomePage.selectAnotherCardCheckout().click();
		// Flujo de pago en Tarjeta de credito
		// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
		for (int i = 0; i <= 2; i++) {
			if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
				HomePage.addCardBtnOnProcessOrder().click();
				HomePage.cardHolderTxt().click();
				HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.backAndroidButton();
				HomePage.cardNumberTxt().click();
				ClickKeyboard(CardNumber);
				ScreenUtils.backAndroidButton();
				HomePage.dueDateTxt().click();
				ClickKeyboard(DueDate);
				ScreenUtils.backAndroidButton();
				HomePage.identifierTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
				HomePage.whiteColorRadioBtn().click();
				HomePage.saveCardBtn().click();
				Thread.sleep(2000);
				// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
				if (HomePage.cvcPinTxt().isDisplayed()) {
					HomePage.cvcPinTxt().sendKeys(CvcNumber);
				}
				HomePage.confirmSaveCardBtn().click();
			}

		}
		// Flujo al tener la tarjeta de credito especifica creada
		ScreenUtils.LongPressClick(HomePage.findCardTxt());
		HomePage.chooseThisCard().click();
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());*/
		HomePage.rechargeBtn().click();
		HomePage.continueBtnRecharge().click();
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
		if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

			ClickKeyboard(CvcNumber);
		}
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
		waitForVisibility(HomePage.seeDetailBtn());
		HomePage.seeDetailBtn().click();
		Thread.sleep(3000);
		ScreenUtils.backAndroidButton();
		ScreenUtils.backAndroidButton();
	}

	public void MandaditosWorkFlow() throws InterruptedException {
		ScreenUtils.SearchService();
		HomePage.specificServiceBtn().click();
		ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
		// Validacion para validar si esciste un raiting pendiente
		if (HomePage.totalRideRaitingBtn().isDisplayed()) {
			// Validacion de que nivel de raiting se le dara al driver
			switch (ratingLvl) {
			case "1":
				HomePage.raitingLvl1Mandaditos().click();
				break;
			case "2":
				HomePage.raitingLvl2Mandaditos().click();
				break;
			case "3":
				HomePage.raitingLvl3Mandaditos().click();
				break;
			case "4":
				HomePage.raitingLvl4Mandaditos().click();
				break;

			}
			HomePage.continueRaiting().click();
			ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
			// Validacion si depende del nivel que se le de al driver, se pueda dar propina
			if (HomePage.tip1Btn().isDisplayed()) {
				switch (rewardMandaditos) {

				case "1":
					HomePage.tip1Btn().click();
					break;
				case "2":
					HomePage.tip2Btn().click();
					break;
				case "3":
					HomePage.tip3Btn().click();
					break;
				default:
					HomePage.tipPlusBtn().click();
					HomePage.qtyPlusTxt().sendKeys(rewardMandaditos);
					HomePage.readyPlusBtn().click();
					break;
				}
				HomePage.rewardDriverMandaditos().click();
			}
			if (HomePage.sendRaitingMandaditos().isDisplayed()) {
				HomePage.sendRaitingMandaditos().click();
			}
		}

		if (HomePage.okBtnMandaditos().isDisplayed()) {
			HomePage.okBtnMandaditos().click();
		}
		waitForVisibility(HomePage.pickUpAddressTxt());
		HomePage.pickUpAddressTxt().click();
		HomePage.pickUpTxt().click();
		HomePage.pickUpTxt().sendKeys(Location);
		waitForVisibility(HomePage.specificLocationBtn());
		HomePage.specificLocationBtn().click();
		HomePage.confirmLocationBtn().click();
		HomePage.descAddressTxt().click();
		HomePage.descAddressTxt().sendKeys(DescAddress);
		HomePage.descriptionShipmentTxt().click();
		HomePage.descriptionShipmentTxt().sendKeys(DescriptionShipment);
		HomePage.saveDescriptionBtn().click();
		HomePage.deliveryAddresTxt().click();
		HomePage.currentLocationBtn().click();
		HomePage.confirmLocationBtn().click();
		HomePage.descAddressTxt().click();
		HomePage.descAddressTxt().sendKeys(DescAddress);
		HomePage.descriptionShipmentTxt().click();
		HomePage.descriptionShipmentTxt().sendKeys(DescriptionShipment);
		HomePage.saveDescriptionBtn().click();
		HomePage.shipmentBtn().click();

		switch (PaymentType) {
		case "Cash":
			for (int i = 0; i < 5; i++) {
				if (!HomePage.cashPaymentMandaditos().isDisplayed()) {
					ScreenUtils.swipeElementByPixels(HomePage.paymentContainer(), "LEFT");
				} else {
					HomePage.cashPaymentMandaditos().click();
				}
			}
			switch (PaymentLocationMandaditos) {
			case "pickup":
				HomePage.payInPickUpLocationBtn().click();
				break;
			case "delivery":
				HomePage.payInDeliveryLocationBtn().click();
				break;
			}
			break;
		case "Card":
			HomePage.paymentContainer().click();
			break;

		}

		ScreenUtils.findItemWithScrollingUsingBy(HomePage.addInvoiceMandaditos(), 10);

		// Validacion para agregar una factura
		if (!Invoice.isEmpty()) {
			waitForVisibility(HomePage.addInvoiceMandaditos());
			HomePage.addInvoiceMandaditos().click();
			// Validacion para verificar si la factura no existe, que la cree
			if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
				HomePage.addInvoiceBtn().click();
				waitForVisibility(HomePage.nameInvoiceTxt());
				HomePage.nameInvoiceTxt().sendKeys(Name);
				HomePage.nitInvoiceTxt().sendKeys(NIT);
				HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
				HomePage.emailInvoiceTxt().sendKeys(Email);
				HomePage.saveInvoiceBtn().click();
				waitForVisibility(HomePage.logoCheckoutImage());
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
				HomePage.selectInvoiceBtn().click();
				HomePage.useInvoiceBtn().click();
			} else {
				// Si existiera la factura solo la seleccione
				HomePage.searchInvoice().click();
				HomePage.selectInvoiceBtn().click();
				HomePage.useInvoiceMandaditosBtn().click();
			}
		}
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.sendShipmentFinalStepBtn(), 10);
		HomePage.sendShipmentFinalStepBtn().click();

		if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

			ClickKeyboard(CvcNumber);
		}
		if (HomePage.changeTxtMandaditos().isDisplayed()) {
			if (NeedChange.isEmpty()) {
				HomePage.changeNotNeedBtn().click();
			} else {
				HomePage.changeTxtMandaditos().sendKeys(NeedChange);
				HomePage.readyBtnMandaditos().click();
			}

		}
		waitForVisibility(HomePage.seeDetailBtn());
		HomePage.seeDetailBtn().click();
		ScreenUtils.backAndroidButton();
		ScreenUtils.backAndroidButton();
	}

	public void HugoCashWorkFlow() throws InterruptedException {
		ScreenUtils.SearchService();
		HomePage.specificServiceBtn().click();
		Thread.sleep(2500);
		// Validacion si aparece un raiting de una orden pasada
		for (int i = 0; i <= 20; i++) {
			if (HomePage.ratingPartner().isDisplayed()) {
				HomePage.ratingPartner().click();
				HomePage.ratingDriver().click();
				HomePage.ratingHugo().click();
				// Validacion de si aparece la propina en un flujo normal de Delivery
				if (HomePage.tipDriver().isDisplayed()) {
					waitForVisibility(HomePage.tipDriver());
					HomePage.tipDriver().click();
					HomePage.tip1Driver().click();
					HomePage.thanksBtnTip().click();
				}
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.skipRaitingBtn(), 20);
				HomePage.skipRaitingBtn().click();
			}
		}
		// Flujo normal de Delivery sin raiting y sin pagar propina
		waitForVisibility(HomePage.titleOnService());
		HomePage.searchTxtOnService().click();
		HomePage.searchElasticOnService().click();
		HomePage.searchElasticOnService().sendKeys(SearchResult);
		waitForVisibility(HomePage.resultOnSearch());
		HomePage.resultOnSearch().click();
		waitForVisibility(HomePage.logoScreenMenu());
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.productSearch(), 100);
		// Validacion de si al buscar un producto, se encuentra un Draft ya realizado,
		// pues lo finalizara
		if (HomePage.viewOrderBtn().isDisplayed()) {
			HomePage.viewOrderBtn().click();
			waitForVisibility(HomePage.productImage());
			HomePage.processOrderBtn().click();
		}
		// Validacion de si no encuentra ni un Draft, continuara con la Orden
		else {
			HomePage.productSearch().click();
			Thread.sleep(3000);
			HomePage.addToOrderBtn().click();
			Thread.sleep(2000);
			// Validacion de que al agregar el producto, nos muestre una alerta indicandonos
			// que ya tenemos un draft y lo queremos eliminar para hacer uno nuevo
			if (HomePage.alertDialog().isDisplayed()) {
				HomePage.alertDialog().click();
				waitForInvisibility(HomePage.loadingRecargasLogo());
			}
			// Validacion para cuando se encuentre en la pantalla del detalle del producto
			// especifico, al seleccionar la opcion de agregar producto nos muestra error,
			// se dara back para visualizar el "Ver Orden"
			if (!HomePage.viewOrderBtn().isDisplayed()) {
				ScreenUtils.backAndroidButton();
			}
			HomePage.viewOrderBtn().click();
			waitForVisibility(HomePage.productImage());
			HomePage.processOrderBtn().click();
		}

		// Flujo de pago en Tarjeta de credito
		// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
		for (int i = 0; i <= 2; i++) {
			if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
				HomePage.addCardInScheduleOrder().click();
				HomePage.cardHolderTxt().click();
				HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.backAndroidButton();
				HomePage.cardNumberTxt().click();
				ClickKeyboard(CardNumber);
				ScreenUtils.backAndroidButton();
				HomePage.dueDateTxt().click();
				ClickKeyboard(DueDate);
				ScreenUtils.backAndroidButton();
				HomePage.identifierTxt().sendKeys(Name + " " + LastName);
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
				HomePage.whiteColorRadioBtn().click();
				HomePage.saveCardBtn().click();
				Thread.sleep(2000);
				// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
				if (HomePage.cvcPinTxt().isDisplayed()) {
					HomePage.cvcPinTxt().sendKeys(CvcNumber);
				}
				HomePage.confirmSaveCardBtn().click();
			}

		}
		// Flujo al tener la tarjeta de credito especifica creada
		HomePage.findCardTxt().click();
		Thread.sleep(3000);
		HomePage.continueBtnPaymentMethod().click();
		waitForVisibility(HomePage.logoCheckoutImage());
		// Validacion si se usara codigo promocional en el checkout
		if (!PromoCode.isEmpty()) {
			waitForVisibility(HomePage.promoCodeBtn());
			HomePage.promoCodeBtn().click();
			HomePage.promoCodeTxt().click();
			ClickKeyboard(PromoCode);
			HomePage.promoReadyBtn().click();
			HomePage.successfullPromoBtn().click();
		}
		// Validacion para ingresar un free delivery

		if (!FreeDeliverys.isEmpty()) {
			Thread.sleep(5000);
			if (HomePage.prizesBtn().isDisplayed()) {
				HomePage.prizesBtn().click();
				HomePage.freeDeliveryBtn().click();
				HomePage.selectAwardBtn().click();
				if (HomePage.successfullPromoBtn().isDisplayed()) {
					HomePage.successfullPromoBtn().click();
				}
			}
		}
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.invoiceBtn(), 20);

		// Validacion para agregar una factura
		if (!Invoice.isEmpty()) {
			waitForVisibility(HomePage.invoiceBtn());
			HomePage.invoiceBtn().click();
			// Validacion para verificar si la factura no existe, que la cree
			if (ScreenUtils.findItemOnList(HomePage.searchInvoice(), 5) == null) {
				HomePage.addInvoiceBtn().click();
				waitForVisibility(HomePage.nameInvoiceTxt());
				HomePage.nameInvoiceTxt().sendKeys(Name);
				HomePage.nitInvoiceTxt().sendKeys(NIT);
				HomePage.addressInvoiceTxt().sendKeys(SearchAddress);
				HomePage.emailInvoiceTxt().sendKeys(Email);
				HomePage.saveInvoiceBtn().click();
				//waitForVisibility(HomePage.logoCheckoutImage());
				ScreenUtils.findItemWithScrollingUsingBy(HomePage.searchInvoice(), 5);
				HomePage.selectInvoiceBtn().click();
				HomePage.useInvoiceBtn().click();
			} else {
				// Si existiera la factura solo la seleccione
				HomePage.selectInvoiceBtn().click();
				HomePage.useInvoiceBtn().click();
			}
		}
		ScreenUtils.findItemWithScrollingUsingBy(HomePage.processOrderStep1(), 20);

		// Validacion si la orden es programada
		if (!OrderType.isEmpty()) {
			waitForVisibility(HomePage.scheduleOrderBtn());
			HomePage.scheduleOrderBtn().click();
			HomePage.btnAgreeScheduleOrder().click();

		}

		// Validacion si se necesita agregar instrucciones
		if (!InstructionsOnCheckout.isEmpty()) {
			waitForVisibility(HomePage.instructionsOnCheckoutTxt());
			HomePage.instructionsOnCheckoutTxt().click();
			HomePage.instructionsOnCheckoutTxt().sendKeys(InstructionsOnCheckout);
			ScreenUtils.backAndroidButton();
		}

		HomePage.processOrderStep1().click();
		// Validacion si se nos solicita el ingreso de CVC al pagar
		if (HomePage.cvcPinPaymentTxt().isDisplayed()) {

			ClickKeyboard(CvcNumber);
		}
		HomePage.confirmLocation().click();
		waitForVisibility(HomePage.goToTrackingBtn());
		HomePage.goToTrackingBtn().click();
		if (OrderType.isEmpty()) {
			ScreenUtils.backAndroidButton();
			ScreenUtils.backAndroidButton();
		}
	}

	public void HugoPayWorkFlow() throws InterruptedException{
		HomePage.hugoPayBtnHome().click();
		Thread.sleep(3000);
		if(HomePage.pinLoginHugoPay().isDisplayed())
		{
			HomePage.pinLoginHugoPay().click();
			ScreenUtils.ClickKeyboard(pinHugoPay);
			// Flujo para entrar a Notifiaciones de HugoPay
			HomePage.notificationsHugoPay().click();
			ScreenUtils.backAndroidButton();
			// Flujo para entrar a Recargas de HugoPay
			HomePage.topUpHugoPay().click();
			ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
			ScreenUtils.backAndroidButton();
			// Flujo para entrar a Pagos de Servicio de HugoPay
			HomePage.pagosServiciosHugoPay().click();
			ScreenUtils.waitForInvisibility(HomePage.loadingRecargasLogo());
			ScreenUtils.backAndroidButton();
			// Flujo para ingresr al tracker dentro de HugoPay
			HomePage.trackerHugoPay().click();
			ScreenUtils.backAndroidButton();
			// Flujo para entrar a las opciones - FAQs dentro de HugoPay
			HomePage.optionsHugoPay().click();
			HomePage.helpHugoPay().click();
			HomePage.faqsHugoPay().click();
			ScreenUtils.backAndroidButton();
			// Flujo para entrar a las opciones - Seguridad dentro de HugoPay
			HomePage.optionsHugoPay().click();
			HomePage.securityHugoPay().click();
			// Cambiar PIN
			HomePage.changePinHugoPay().click();
			HomePage.changePinHugoPaytxt().sendKeys(pinHugoPay);;
			HomePage.nextChangePinBtn().click();
			HomePage.changePinHugoPaytxt().sendKeys(pinHugoPay);
			HomePage.nextChangePinBtn().click();
			// Flujo para entrar a las opciones - Agregar Tarjeta dentro de HugoPay
			HomePage.optionsHugoPay().click();
			HomePage.addCardHugoPay().click();
			// Flujo de pago en Tarjeta de credito
			// Validacion de si se encuentra una tarjeta en especifico y si no, se crea.
			for (int i = 0; i <= 2; i++) {
				if (ScreenUtils.findItemOnList(HomePage.findCardTxt(), 5) == null) {
					HomePage.addCardOnHugoPay().click();
					HomePage.cardHolderTxt().click();
					HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.backAndroidButton();
					HomePage.cardNumberTxt().click();
					ClickKeyboard(CardNumber);
					ScreenUtils.backAndroidButton();
					HomePage.dueDateTxt().click();
					ClickKeyboard(DueDate);
					ScreenUtils.backAndroidButton();
					HomePage.identifierTxt().sendKeys(Name + " " + LastName);
					ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
					HomePage.whiteColorRadioBtn().click();
					HomePage.saveCardBtn().click();
					Thread.sleep(2000);
					// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
					if (HomePage.cvcPinTxt().isDisplayed()) {
						HomePage.cvcPinTxt().sendKeys(CvcNumber);
					}
					HomePage.confirmSaveCardBtn().click();
				}

			}
			// Flujo al tener la tarjeta de credito especifica creada
			ScreenUtils.LongPressClick(HomePage.findCardTxt());
			HomePage.selectCardHugoPay().click();
			Thread.sleep(3000);
			// Flujo para levantar un QR especificamente.
			for(int i = 0; i<20; i++)
			{
			// Validacion para buscar una tarjeta por nombre de indentifacion
			if(findItemUsingBy2(HomePage.listCardHugoPay2(), HomePage.findCardTxt(), 10)== null)
			{
			HomePage.qrCodeBtnHugoPay().click();
			HomePage.addCardOnHugoPayQr().click();
			HomePage.addCardOnHugoPay().click();
			HomePage.cardHolderTxt().click();
			HomePage.cardHolderTxt().sendKeys(Name + " " + LastName);
			ScreenUtils.backAndroidButton();
			HomePage.cardNumberTxt().click();
			ClickKeyboard(CardNumber);
			ScreenUtils.backAndroidButton();
			HomePage.dueDateTxt().click();
			ClickKeyboard(DueDate);
			ScreenUtils.backAndroidButton();
			HomePage.identifierTxt().sendKeys(Name + " " + LastName);
			ScreenUtils.findItemWithScrollingUsingBy(HomePage.saveCardBtn(), 20);
			HomePage.whiteColorRadioBtn().click();
			HomePage.saveCardBtn().click();
			Thread.sleep(2000);
			// Validacion si se nos solicita el ingreso de CVC al crear una tarjeta
			if (HomePage.cvcPinTxt().isDisplayed()) {
				HomePage.cvcPinTxt().sendKeys(CvcNumber);
			}
			HomePage.confirmSaveCardBtn().click();
			ScreenUtils.backAndroidButton();
			ScreenUtils.backAndroidButton();
			}
			// Validacion de que al encontrar la tarjeta levante el QR especifico
			else
			{ 
				HomePage.findCardTxt().click();
				break;
			}
			}
			
		}
	}
	
	public void deleteFile() {
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder2 = newpath + "\\src\\main\\resources\\env\\test.properties";
		File file= new File(otherFolder2); 
		file.delete();
	}
	public void renameFile(int i) {
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder1 = newpath + "\\src\\main\\resources\\env\\test"+i+".properties";
		String otherFolder2 = newpath + "\\src\\main\\resources\\env\\test.properties";
		File file = new File(otherFolder1);
	    File rename = new File(otherFolder2);
	    file.renameTo(rename);
	}
	
	public void replacePaths(int i) throws InterruptedException{
		String path = new File("").getAbsolutePath();
		String newpath = path.replace("\\", "\\\\");
		String otherFolder1 = newpath + "\\src\\main\\resources\\env2\\test"+i+".properties";
		String otherFolder2 = newpath + "\\src\\main\\resources\\env";
		copyFile(otherFolder1,otherFolder2);
	}
	
	public static void PopulateVariables() {
		Country = System.getProperty("Country");
        Phone =  System.getProperty("Phone");
        Password =  System.getProperty("Password");
    	Name = System.getProperty("Name");
    	LastName = System.getProperty("LastName");
    	Email = System.getProperty("Email");
    	Location = System.getProperty("Location");
    	DescAddress = System.getProperty("DescAddress");
    	DescriptionShipment = System.getProperty("DescriptionShipment");
    	Service = System.getProperty("Service");
    	SearchResult = System.getProperty("SearchResult");
    	Product = System.getProperty("Product");
    	Provider = System.getProperty("Provider");
    	NumberContract = System.getProperty("NumberContract");
    	HouseNumber = System.getProperty("HouseNumber");
    	CardNumber = System.getProperty("CardNumber");
    	DueDate = System.getProperty("DueDate");
    	CvcNumber = System.getProperty("CvcNumber");
    	PaymentType = System.getProperty("PaymentType");
    	PromoCode = System.getProperty("PromoCode");
    	FreeDeliverys = System.getProperty("FreeDeliverys");
    	Invoice = System.getProperty("Invoice");
    	NIT = System.getProperty("NIT");
    	SearchAddress = System.getProperty("SearchAddress"); 
    	InstructionsOnCheckout = System.getProperty("InstructionsOnCheckout");
    	NeedChange = System.getProperty("NeedChange");
    	Amount = System.getProperty("Amount");
    	PaymentLocationMandaditos = System.getProperty("PaymentLocationMandaditos");
    	ratingLvl = System.getProperty("ratingLvl");
    	rewardMandaditos = System.getProperty("rewardMandaditos");
    	OrderType = System.getProperty("OrderType");
    	SendCode = System.getProperty("SendCode");
    	pinHugoPay = System.getProperty("pinHugoPay");
    	Event = System.getProperty("Event");
    	Service = System.getProperty("Service");
    	Address = System.getProperty("Address");
    	SearchResult = System.getProperty("SearchResult");
    	Product = System.getProperty("Product");
    	CategoryHugoMarket = System.getProperty("CategoryHugoMarket");
    	PhoneOperator = System.getProperty("PhoneOperator");
    	ServicePagoAServicio = System.getProperty("ServicePagoAServicio");
    	Provider = System.getProperty("Provider");
    	TypeOfContract = System.getProperty("TypeOfContract");
    	Name = System.getProperty("Name");
    	LastName = System.getProperty("LastName");
    	Category = System.getProperty("Category");
    	Event = System.getProperty("Event");
    	Last4NumberCard = System.getProperty("Last4NumberCard");
	}
	
	public static void CleanVariables() {
		
		Country = "";
        Phone =  "";
        Password =  "";
    	Name = "";
    	LastName = "";
    	Email = "";
    	Location = "";
    	DescAddress = "";
    	DescriptionShipment = "";
    	Service = "";
    	SearchResult = "";
    	Product = "";
    	Provider = "";
    	NumberContract = "";
    	HouseNumber = "";
    	CardNumber = "";
    	DueDate = "";
    	CvcNumber = "";
    	PaymentType = "";
    	PromoCode = "";
    	FreeDeliverys = "";
    	Invoice = "";
    	NIT = "";
    	SearchAddress = ""; 
    	InstructionsOnCheckout = "";
    	NeedChange = "";
    	Amount = "";
    	PaymentLocationMandaditos = "";
    	ratingLvl = "";
    	rewardMandaditos = "";
    	OrderType = "";
    	SendCode = "";
    	pinHugoPay = "";
    	Event = "";
    	Service = "";
    	Address = "";
    	SearchResult = "";
    	Product = "";
    	CategoryHugoMarket = "";
    	PhoneOperator = "";
    	ServicePagoAServicio = "";
    	Provider = "";
    	TypeOfContract = "";
    	Name = "";
    	LastName = "";
    	Category = "";
    	Event = "";
    	Last4NumberCard = "";
	}
	
	
}
