
import static util.ScreenUtils.backAndroidButton;
import static util.ScreenUtils.waitForVisibility;


import org.testng.annotations.Test;

import pages.HomePage;
import pages.Profile;
import util.ScreenUtils;

public class ProfileTest extends BaseClass {

	@Test()
	public void profileTest() throws InterruptedException {
		
		try {
		// Se instancia y dispara el Test de Inicio de sesion para poder tener una
		// sesion activa
		// SignInTest Login = new SignInTest();
		// Login.LogInTest();
		// Se inicia el Test de Profile
			if(HomePage.clevertapDialog().isDisplayed())
			{
				backAndroidButton();
			}
		waitForVisibility(Profile.profileBtn());
		Profile.profileBtn().click();
		
		//Profile.editProfileBtn().click();
		// Se ingresa para editar la foto, nombre y email
		/*Profile.editImageProfileBtn().click();
		Thread.sleep(2000);
		if(Profile.permisionDialogBrowserstack().isDisplayed())
		{
		Profile.permisionDialogBrowserstack().click();
		}
		Thread.sleep(2000);
		if(Profile.galleryBtn().isDisplayed())
		{
			Profile.galleryBtn().click();
		}
		Profile.pictureBtn().click();
		Profile.selectImageGallery().click();
		Profile.readyBtn().click();
		Profile.fullNameProfileTxt().clear();
		Profile.fullNameProfileTxt().sendKeys(Name + " " + LastName);
		Profile.emailProfileTxt().clear();
		Profile.emailProfileTxt().sendKeys(Email);
		Profile.updateProfileBtn().click();
		
		// Se ingresa agregar una direccion y eliminarla
		Profile.addressBtn().click();
		Profile.btnAddAddress().click();
		Thread.sleep(2500);
		Profile.confirmLocationBtn().click();
		Profile.numberHouseTxt().sendKeys(HouseNumber);
		Profile.saveAddressBtn().click();
		Profile.confirmSaveAddressBtn().click();
		if(Profile.deleteAddressBtn().isDisplayed())
		{
			Profile.deleteAddressBtn().click();
			Profile.confirmDeleteBtn().click();
			Profile.backAddressBtn().click();
		}
		
		
		// Se ingresa agregar una tarjeta de credito y eliminarla
		waitForVisibility(Profile.paymentsBtn());
		Profile.paymentsBtn().click();
		Profile.addCardBtn().click();
		Profile.cardHolderTxt().click();
		Profile.cardHolderTxt().sendKeys(Name + " " + LastName);
		ScreenUtils.backAndroidButton();
		Profile.cardNumberTxt().click();
		ClickKeyboard(CardNumber);
		ScreenUtils.backAndroidButton();
		Profile.dueDateTxt().click();
		ClickKeyboard(DueDate);
		ScreenUtils.backAndroidButton();
		Profile.identifierTxt().sendKeys(Name + " " + LastName);
		ScreenUtils.findItemWithScrollingUsingBy(Profile.saveCardBtn(), 20);
		Profile.whiteColorRadioBtn().click();
		Profile.saveCardBtn().click();
		Profile.cvcPinTxt().sendKeys(CvcNumber);
		Profile.confirmSaveCardBtn().click();
	*/
		
		// Flujo para eliminar una TC
		
		Profile.paymentsBtn().click();
		//ScreenUtils.findItemWithScrollingUsingBy(Profile.findCardTxt(), 20);
		//Profile.findCardTxt().click();

		ScreenUtils.LongPressClick(Profile.findCardTxt());
		Profile.removeCardBtn().click();
		Profile.confirmSaveCardBtn().click();
		Profile.backPaymentsBtn().click();

		// Cambiar contraseņa, por alguna razon de momento no funciona, se estara
		// fixeando luego.
		/*
		 * Profile.preferencesBtn().click(); 
		 * Profile.changeMyPasswordBtn().click();
		 * Profile.changePasswordTxt().click(); 
		 * Profile.changePasswordTxt().click();
		 * Thread.sleep(5000); 
		 * Profile.changePasswordTxt().sendKeys(Password);
		 * Profile.confirmChangePasswordTxt().sendKeys(Password);
		 * Profile.savePasswordBtn().click();
		 */
		
		}
		catch (Exception e)
		{
			System.out.println("Error: " +e);
		}
	}

}
