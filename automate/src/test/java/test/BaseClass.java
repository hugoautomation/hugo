package test;

import static configuration.Config.CONFIG;
import static driver.AppiumServer.APPIUM_SERVICE;
import static driver.DriverManager.DRIVER;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import util.ScreenUtils;

import com.codeborne.selenide.WebDriverRunner;

import driver.MongoDbDriver;
import io.appium.java_client.android.AndroidDriver;


public class BaseClass extends MongoDbDriver
{
    @BeforeSuite(alwaysRun = true)
    public void beforeTest(ITestContext context) {
        CONFIG.setTestEnvironment();
        DRIVER.setDriver();
        
        WebDriverRunner.setWebDriver(DRIVER.getDriver());
        ConnectMongoDB();
        
    }
	
	public void waitForVisibility(WebElement webElement) {
		  WebDriverWait wait = new WebDriverWait(DRIVER.getDriver(), ScreenUtils.WAIT);
		  wait.until(ExpectedConditions.visibilityOf(webElement));
		  
	  }
	
	@SuppressWarnings("deprecation")
	public void ClickKeyboard() {
		DRIVER.getDriver().getKeyboard().sendKeys("0");
		  
	  }
	@SuppressWarnings("rawtypes")
	public void readOTP() throws InterruptedException
	{
		((AndroidDriver) DRIVER.getDriver()).openNotifications();
		Thread.sleep(45000);
		String otp = DRIVER.getDriver().findElementByXPath("//*[contains(@text,'hugoCode')]").getText().split("hugoCode:" )[0];
	    System.out.println(otp);
	}
	

    @AfterSuite(alwaysRun = true)
    public void afterTest(final ITestContext testContext) {
        DRIVER.closeDriver();
        APPIUM_SERVICE.stop();
    }
}
